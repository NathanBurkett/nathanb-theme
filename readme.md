# NathanB.me

**Authors**:

* [Nathan Burkett](https://github.com/nathanburkett) // [http://www.nathanb.me.com](http://www.nathanb.me.com) // [@nathan_burkett](https://twitter.com/nathan_burkett)

**Version**: 0.5.0
**Wordpress version tested to**: 4.0
**License**: GPLv2 

## Description

Wordpress Theme built for NathanB.me

## Changelog

### 0.5
* Moved APIs to PHP
* Finished Styling
* Added Loading Animations

### 0.4
* Added APIs
- Twitter
- Instagram
- Github
- Last.FM

### 0.3
* Build Schema data in to HTML Outline
* Build HTML Outline for Content in header.php, index.php, footer.php
* Fix Sass Conflicts
* Remove Unnecessary Files / Functions / Folders
* Add Font-Face and typefaces

### 0.2
* Initial release

## To Do
* Localize content
* Add Google Analytics -> Via Site Options
* Change favicon styles to NathanB styles
* Add Admin Options Panel
* Re-build 404.php for BEM
