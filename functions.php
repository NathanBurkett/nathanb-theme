<?php

/**
 * FUNCTIONS.PHP
 * Main functions interacting with theme
 *
 * @author Roadwork Rah, LLC
 *         Nathan Burkett <nathan@roadworkrah.com> (@nathan_burkett)
 *
 * @since 0.2
 *
 * @see library/theme-contents.txt  For a location of files and functions
 *
 * @since 0.2
 */


/**
 * INCLUDE NEEDED FILES
 * These files are kept in separate directories to keep them better organized
 *
 * @todo Upload-dir_new.php has $post_id erased while CMB2 is active
 * @since 0.2
 */

require_once('library/theme-support.php');
require_once('library/admin.php');
require_once('library/translation/translation.php');
require_once('library/shortcodes/shortcodes.php');



/**
 * INIT
 * @since 0.2
 */ 

function rah_theme_init() {

  load_theme_textdomain( 'rah_theme', get_template_directory() . '/library/translation' );

  //** Launching Operation Cleanup
  add_action( 'init', 'rah_head_cleanup' );
  //** A Better Title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  //** Remove WP Version From RSS
  add_filter( 'the_generator', 'rah_rss_version' );
  //** Remove Pesky Injected CSS For Recent Comments Widget
  add_filter( 'wp_head', 'rah_remove_wp_widget_recent_comments_style', 1 );
  //** Clean Up Comment Styles In The Head
  add_action( 'wp_head', 'rah_remove_recent_comments_style', 1 );
  //** Clean Up Gallery Output In WP
  add_filter( 'gallery_style', 'rah_gallery_style' );

  //** Enqueue Base Scripts And Styles
  add_action( 'wp_enqueue_scripts', 'rah_scripts_and_styles', 999 );
  //** Ie Conditional Wrapper

  //** Launching This Stuff After Theme Setup
  rah_theme_support();

  //** Adding Sidebars To Wordpress (these Are Created In Functions.php)
  // add_action( 'widgets_init', 'rah_register_sidebars' );

  //** Cleaning Up Excerpt
  add_filter( 'excerpt_more', 'rah_excerpt_more' );
  //** Remove WPAUTOP Filter
  remove_filter( 'the_content', 'wpautop' );

  //** Alter the WPAUTOP filter
  // add_filter('the_content', 'roadwork_fix_shortcode_empty_paragraph');
  // add_filter('the_excerpt', 'roadwork_fix_shortcode_empty_paragraph');
  // add_filter('widget_text', 'roadwork_fix_shortcode_empty_paragraph');

  //** Remove The Default Post Editor From Certain Templates
  // add_action( 'admin_init', 'remove_editor' );

}

//** BLASTOFF
add_action( 'after_setup_theme', 'rah_theme_init' );



/**
 * oEMBED SIZE OPTIONS
 * @since 0.2
 */

if ( ! isset( $content_width ) ) {
  $content_width = 640;
}


/**
 * ADD IMAGE SIZE
 * Register a new image size.
 *
 * Cropping behavior for the image size is dependent on the value of $crop:
 * 1. If false (default), images will be scaled, not cropped.
 * 2. If an array in the form of array( x_crop_position, y_crop_position ):
 *    - x_crop_position accepts 'left' 'center', or 'right'.
 *    - y_crop_position accepts 'top', 'center', or 'bottom'.
 *    Images will be cropped to the specified dimensions within the defined crop area.
 * 3. If true, images will be cropped to the specified dimensions using center positions.
 *
 * @param string     $name   Image size identifier.
 * @param int        $width  Image width in pixels.
 * @param int        $height Image height in pixels.
 * @param bool|array $crop   Optional. Crop images to specified height and width or resize.
 *                           An array can specify positioning of the crop area. Default false.
 * @return bool|array False, if no image was created. Metadata array on success.
 *
 * @since 0.2
 */

// banner


// square


// double-wide

add_image_size( 'person-thumb', 72, 72, true );
add_image_size( 'person-thumb-2x', 144, 144, true );
add_image_size( 'person-thumb-3x', 216, 216, true );

add_filter( 'image_size_names_choose', 'rah_custom_image_sizes' );



/**
 * CUSTOM IMAGE SIZES
 * Add custom sizes to the media selector in post edit screen in WP Admin
 *
 * Must use the name identifier from add_image_size for actual dimensions
 *
 * @param string      $key    Image size indentifier in add_image_size();
 * @param string      $value  Description of image size (px x px most understandable)
 * 
 * @since 0.2
 */

function rah_custom_image_sizes( $sizes ) {
  return array_merge( $sizes, array(
    'person-thumb' => __('Avatar Size'),
  ) );
}




/**
 * REMOVE HEIGHT ATTRIBUTE FROM IMGS
 * @since 0.2
 */ 

function remove_height_attribute( $html ) {
   $html = preg_replace( '/(height)="\d*"\s/', "", $html );
   return $html;
}

add_filter( 'post_thumbnail_html', 'remove_height_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_height_attribute', 10 );


/** 
 * ARRAY COUNT VALUES OF
 * Count number of times string occurs in array
 *
 * @param  string $needle
 * @param  array  $haystack
 * @return Number of occurrences
 * @since 0.2
 */

function array_count_values_of($needle, $array) {
    $count_arr = array_count_values($array);
    return $count_arr[$value];
}


/**
 * LOADER
 * @since 0.2
 */ 

function get_loader() {
  $a = get_svg_icon("loader", "icon--loader");
  return "<div id='js--loader' class='media--loader'>".$a."</div>";
}


/**
 * AJAX POST LOOP
 *
 * @param string  $container  Targeting container to insert posts into
 * @param string  $post_type  Post type for query
 * @param int     $num        Number of posts to show. If query returns less than $num, actual
 *                            number shown
 * @var   string  $ajax_var   Protected class variable for gathering and outputting ajax script tags
 *                            in page footer
 * @see library/js/functions/ajax.getPost.js
 * @return string Returns the js--loader that is replaced by the ajaxGetPosts func
 *
 *
 * @since 0.2
 */ 

class ajax_posts {

  protected static $ajax_var = '';

  public static function get_ajax_posts($container, $post_type, $num) {

    $script = "<script>
    jQuery(".$container.")ajaxGetPosts(".$post_type.", ".$num.");
    </script>";

  self::$ajax_var[] = $script;

  add_action( 'wp_footer', array ( __CLASS__, 'add_ajax_posts' ), 20 );

  return '<div id="js--loader" class="media--loader" ></div>';

}

  public static function add_ajax_posts() {
    foreach( self::$ajax_var as $script ){
      echo $script;
    }
  }
}




/**
 * SVG FUNCTION
 *
 * @param string      $path
 * @param string|null $classes  Classes to add to SVG elem
 * @return string SVG HTML element
 * @since 0.2
 */ 

function get_svg($path, $classes = null) {
  $class_attr = $classes ? " class=\"$classes\"" : "";
  return "<svg{$class_attr}><use xlink:href=\"$path\"></use></svg>";
}

function get_svg_icon($id, $classes = null) {
  $classes = $classes ? $classes : "icon icon--$id";
  return get_svg(get_stylesheet_directory_uri().'/library/images/icons.svg#icon_'.$id, $classes);
}


/**
 * CUSTOM META FUNCTONS
 * @since 0.2
 */ 

/**
 * GET CUSTOM META
 * Get custom meta from meta name
 *
 * @param string $meta_name     Name of custom meta box without the unique '_rah_'
 * @var global   $header_check  Checks meta name for '_header_content' for dynamic filtering
 * @return mixed[] Values, if any, associated with the meta name
 *
 * @since 0.2
 */ 

$header_check = false;

function get_custom_meta($meta_name) {
  global $header_check;
  if ($meta_name):
    $header_check = preg_match("#_header_content#", $meta_name) ? true : false;
    $meta_name = '_rah_'.$meta_name;
    return get_post_meta( get_the_ID(), $meta_name, true );
    $header_check = false;
  else:
    trigger_error(__("Meta name must be set to use get_custom_meta"), E_USER_ERROR);
  endif;
}


/**
 * GET ATTACHMENT IMG
 * Gets the img from file or filelist custom meta types
 * ID for file is stored in meta_name . '_id' ie. 'post_header_bg_id'
 *
 * @param int          $id    ID of attachment to retrieve
 * @param string|null  $size  Optional. Thumnbnail image size identifier. See add_image_size()
 * @return array [0] => src, [1] => width, [2] => height
 *
 * @since 0.2
 */

function get_attach_img($meta_name, $size=null) {
  if ($meta_name) :
    $meta_name = '_rah_'.$meta_name;
    $id = get_attach_ID($meta_name);
    if ($id) :
      $size ? $size : 'full';
      return wp_get_attachment_image($id, $size );
    else :
      trigger_error(__($meta_name . " is not a valid meta field"), E_USER_ERROR);
    endif;
  else :
    trigger_error(__("You must set the meta field to use get_attach_img"), E_USER_ERROR);
  endif;
}


/**
 * IMPROVED CONTENT OUTPUT
 * Remove double line breaks from WYSIWYG output
 *
 * @param string  $content  WYSIWYG Content to be formatted
 * @return HTML
 *
 * @since 0.2
 */

function imp_content_output($content) {
  return wpautop(do_shortcode($content),false);
}


/**
 * WPCF7 AJAX LOADER
 * Switch the Contact Form 7 loader/working symbol to ours
 * @since 0.2
 */ 

// add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');
function my_wpcf7_ajax_loader () {
  return  get_svg_icon('loader', 'icon--loader');
}


/**
 * WPCF7 ALLOW SHORTCODES
 * Allow shortcodes in Contact Form 7 forms
 * @since 0.2
 */ 

add_filter( 'wpcf7_form_elements', 'wpcf7_allow_shortcodes' );

function wpcf7_allow_shortcodes( $form ) {
$form = do_shortcode( $form );
return $form;
}


/**
 * ADD DEFER || ASYNC ATTR TO SCRIPTS
 * Add defer or async attributes to HTML script elements. Automatically outputs on client side
 *
 * @param string  $url  Url to be checked and formatted
 * @return string  Url with defer or async attr if applicable
 *
 * @since 0.2
 */ 

add_filter( 'clean_url', 'add_enqueue_attrib', 11, 1 );

function add_enqueue_attrib( $url ) {
  if ( FALSE === strpos( $url, '.js?defer' )
    or FALSE === strpos( $url, '.js?async')
    )
    { // not our file
    return $url;
    }
    $action = substr($url, -7);
    return "$url' $action='$action";
}


/**
 * DEBUG TO CONSOLE
 * Creates script tag with php variable/array/func output for debugging without interrupting
 * HTML elements
 *
 * @param string|array $data Information being output to console for debugging
 * @return string HTML script element with necessary information inside
 *
 *
 * @since 0.2
 */

function debug_to_console($data) {
    if(is_array($data) || is_object($data))
  {
    echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
  } else {
    echo("<script>console.log('PHP: ".$data."');</script>");
  }
}
    

/**
 * FIX EMPTY P TAGS ON SHORTCODES
 * Remove <p> tags and <br> tags around shortcodes from WYSIWYG
 *
 * @param string  $content  Automatically populated by wordpress via filter use
 * @return string Formatted content
 * 
 * @since 0.2
 */

function roadwork_fix_shortcode_empty_paragraph( $content ) {
   $array = array (
    '<p>['  => '[',
    ']</p>'   => ']',
    ']<br />' => ']',
    ']<br>'   => ']'
   );
   $content = strtr($content, $array);
   return $content;
}


?>