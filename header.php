<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title( '|', true, 'right' ); ?></title>

		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<?php $favicon_dir = get_template_directory_uri().'/library/images/favicon/'; ?>
		<link rel="apple-touch-icon" href="<?php echo $favicon_dir ?>apple-icon-touch.png">
		<link rel="icon" href="<?php echo $favicon_dir ?>favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo $favicon_dir ?>favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo $favicon_dir ?>win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); // wordpress head functions - don't remove ?>

	</head>

	<body <?php body_class(); ?>>

		<main id="container" role="main">

			<header class="row--full-width page__header" role="banner" itemscope itemtype="https://schema.org/Person">

				<div class="row header__content">

					<div class="avatar--main js--load__block"><img src="<?php echo get_template_directory_uri().'/library/images/avatar.jpg'; ?>" class="header__img" alt="<?php _e('Nathan Burkett - Web & Visual Designer - Front-End Developer', 'nb'); ?>" itemprop="image"></div>

					<div class="content__section js--load__block">

						<h1 class="header__title" itemprop="name">Nathan Burkett</h1>

						<p class="header__subtitle" itemprop="jobTitle"><?php _e('Web &amp; Visual Designer', 'nb'); ?></p>

						<p class="header__subtitle" itemprop="jobTitle"><?php _e('Front-End Developer', 'nb'); ?></p>

					</div>

					<ul class="list--single group--link js--load__block">

						<li class="list__item"><?php echo get_svg_icon("talk"); ?><a href="#" class="link__item" itemprop="email">nathan@nathanb.me</a></li>

						<li class="list__item"><?php echo get_svg_icon("twitter"); ?><a href="#" class="link__item" itemprop="url" rel="me">@nathan_burkett</a></li>

						<!-- <li class="list__item"><?php echo get_svg_icon("facebook"); ?><a href="#" class="link__item" itemprop="url" rel="me">facebook.com/NateBurkett</a> -->

						<li class="list__item"><?php echo get_svg_icon("google-plus"); ?><a href="#" class="link__item" itemprop="url" rel="me">google.com/NateBurkett</a></li>

						<li class="list__item"><a href="#" class="link__item js--modal"><?php _e('See commit history for this theme', 'nb'); ?></a></li>

					</ul>

				</div>

				<section class="row--full-width content--intro js--load__block">

					<div class="row">

						<p itemprop="description">An individual with front-lines experience, Nathan excelled in conceptualizing, building, and implementing web platforms for clients in a multitude of ranges and sizes. He built brands from scratch, developed responsive Wordpress themes from bare bones, and fulfilled deliverables and/or site launches on time.  His duties almost always involved project management and maintaining a clean, dynamic, and on-the-cusp code base.</p>

					</div>

				</section>

			</header>
