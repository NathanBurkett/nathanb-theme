			<footer class="row--full-width page__footer" role="contentinfo">

				<div class="row js--load__block footer__content">

					<a href="#">@nathan_burkett</a>

					<a href="#">nathan@nathanb.me</a>

					<p>Copyright 2014 - Nathan Burkett</p>

				</div>

			</footer>

		</main>

		<?php // $analytics = get_admin_option('ga_account'); // start analytics ?>

<!-- 		<?php if ((!$user_ID)&&($analytics)): ?>

			<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', '<?php echo $analytics; ?>', 'auto');  // Replace with your property ID.
			ga('send', 'pageview');

			</script>

		<?php endif; // end analytics ?> -->

		<?php wp_footer(); ?>

	</body>

</html>