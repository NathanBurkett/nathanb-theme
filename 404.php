<?php	get_template_part( 'scaffold/template/standard', 'start' ); ?>

	<article id="post--not-found" class="row">

		<header class="article-header">

			<h1><?php _e("Article Not Found", "rah_theme"); ?></h1>

		</header>

		<section class="entry-content">

			<p><?php _e("Seems you've found something that doesn't exist.", "rah_theme"); ?></p>

		    <p><?php _e("Here are some of our more useful links:", "rah_theme"); ?></p>

		    <a href="<?php echo home_url(); ?>" title="Link to Home Page" alt="Link to Home Page from 404">Homepage</a>

		    <nav role="navigation">

		    	<?php rah_main_nav(); ?>

		    </nav>

		    <p><?php _e("Or try searching the site:", "rah_theme"); ?></p>

		    <p><?php get_search_form(); ?></p>

		</section>

	</article>

<?php get_template_part( 'scaffold/template/standard', 'end'); ?>
