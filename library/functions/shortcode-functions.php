<?php

add_filter('the_content', 'roadwork_fix_shortcode_empty_paragraph');
add_filter('the_excerpt', 'roadwork_fix_shortcode_empty_paragraph');
add_filter('widget_text', 'roadwork_fix_shortcode_empty_paragraph');
// and change excerpt brackets to avoid conflict with empty paragraphs
add_filter('excerpt_more', 'roadwork_change_excerpt_brackets');

function roadwork_fix_shortcode_empty_paragraph( $content ) {
   $array = array (
	  '<p>['	=> '[',
	  ']</p>' 	=> ']',
	  ']<br />' => ']',
	  ']<br>' 	=> ']'
   );
   $content = strtr($content, $array);
   return $content;
}

function roadwork_change_excerpt_brackets( $more ) {
	return '...';
}

?>