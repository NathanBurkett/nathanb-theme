<?php

function column( $atts, $content = null ) {

    extract( shortcode_atts( array( 'size' => '', 'position' => '', 'offset' => '' ), $atts ) );

    if ( $position == 'first' || $position == 'only' ) {

        $column .= '<div class="twelvecol first small-section clearfix">';

    }

    if ( !empty($offset) && !empty($position) || !empty($offset) && empty($position)  ) {

        $column .= '<div class="' . $size .'col offset' . $offset . '">' . do_shortcode( $content ) . '</div>';

    } elseif ( empty($offset) && $position == 'only' ) {

        $column .= '<div class="' . $size .'col first">' . do_shortcode( $content ) . '</div>';

    } elseif ( empty($offset) && $position == 'first' || 'last' ) {

        $column .= '<div class="' . $size .'col ' . $position . '">' . do_shortcode( $content ) . '</div>';

    } elseif ( empty($offset) && empty($position) ) {

        $column .= '<div class="' . $size .'col">' . do_shortcode( $content ) . '</div>';

    }

    if ( $position == 'last' || $position == 'only' ) {

        $column .= '</div>';

    }

    return $column;

}

add_shortcode('column', 'column');
?>