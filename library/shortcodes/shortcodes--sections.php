<?php

$section_depth = 0;

$block__img = false;

$section_type = false;

/*********************
 HEADERS
*********************/

function header_section($atts, $content = null ) {
	extract(shortcode_atts(array( "class" => ""), $atts));

	global $section_depth, $section_type;

	$class_arr = [];

	$section_type = 'header';

	switch($section_depth) :

		case 1:

			$class_arr[] = 'section__header';

		break;

		case 2:

			$class_arr[] = 'content__header';

		break;

		default:

			$class_arr[] = 'block__header';

	endswitch;

	$class ? $class_arr[] = trim($class) : '';

	$classes = implode(" ", $class_arr);

	$output =  "<header class=\"$classes\">".do_shortcode($content)."</header>";

	unset($section_type);

	return $output;

}

add_shortcode('header', 'header_section');


/*********************
 TITLES
*********************/

function title($atts, $content = null ) {
	extract(shortcode_atts(array( "class" => "" ), $atts));

	$type = $start = $end = $attrib = '';

	global $section_depth, $section_type;

	$class_arr = [];

	switch($section_depth) :

		case 0:

			$type = "h1";

			$class_arr[] = 'page__title';

			$attrib = " itemprop=\"headline\"";

		break;

		case 1:

			$type = "h2";

			$class_arr[] = 'section__title';

		break;

		case 2:

			$type = "h3";

			$class_arr[] = 'content__title';

		break;

		default:

			$type = "h4";

			$class_arr[] = 'item__title';

	endswitch;

	$section_type == 'list' ? $class_arr[0] = 'list__title' : '';

	if (strpos($class, 'title') !== false) : unset($class_arr[0]); endif;

	$class ? $class_arr[] = trim($class) : '';

	$classes = implode(" ", $class_arr);

	$content = format_title_symbols($content);

	return "<$type class=\"$classes\"$attrib>".do_shortcode($content)."</$type>";

}

add_shortcode('title', 'title');


function subtitle($atts, $content = null) {
	extract(shortcode_atts(array( "class" => "" ), $atts));

	$class = $class ? strpos($class, " ") === 0 ? $class : " $class" : "";

	$content = format_title_symbols($content);

	return "<span class=\"title--sub subtitle$class\">".do_shortcode($content)."</span>";
}

add_shortcode("subtitle", "subtitle");


/*********************
 SECTIONS
*********************/

function section( $atts, $content = null ) {

	extract(shortcode_atts(array( "class" => "", "type" => "", "feed" => "", "background" => "", "showcase" => "" ), $atts));

	global $section_depth, $block__img;

	++$section_depth;

	$class_arr = [];

	$style_arr = [];

	$class_arr[] = "section__content";

	$class ? $class_arr[] = $class : '';

	$type ? $class_arr[] = get_section_type($type) : "";

	$showcase ? $class_arr[] = "showcase--$showcase" : "";

	if($type == 'img-content') : $block__img = true; endif;

	$feed ? $class_arr[] = "block__feed feed--$feed" : "";

	if ($background) :
		if(strpos($background, "#") == true) :
			$style_arr[] = "background-color: $background;";
		else :
			$class_arr[] = "background--$background";
		endif;
	endif;

	$classes = implode(" ", $class_arr);

	$output = "<section class=\"$classes\">".do_shortcode($content)."</section>"; 

	$block__img = false;

	unset($class_arr);

	$section_depth--;

	return $output;

}

add_shortcode('section', 'section');


function content__section( $atts, $content = null ) {

	extract(shortcode_atts(array( "class" => "", "type" => "", "feed" => "", "background" => "", "showcase" => "" ), $atts));

	global $section_depth, $block__img;

	++$section_depth;

	$class_arr = [];

	$class_arr[] = "content__section";

	$class ? $class_arr[] = $class : '';

	$type ? $class_arr[] = get_section_type($type) : "";

	$showcase ? $class_arr[] = "showcase--$showcase" : "";

	if($type == 'img-content') : $block__img = true; endif;

	$feed ? $class_arr[] = "block__feed feed--$feed" : "";

	$background ? $class_arr[] = "background--$background" : "";

	$classes = implode(" ", $class_arr);

	debug_to_console($classes);

	$output = "<section class=\"$classes\">".do_shortcode($content)."</section>";

	$block__img = false;

	unset($class_arr);

	$section_depth--;

	return $output;

}

add_shortcode('section_sub', 'content__section');


function content( $atts, $content = null ) {
	extract(shortcode_atts(array( "class" => "", "type" => "", "feed" => "", "background" => "", "showcase" => "" ), $atts));

	global $section_depth;

	++$section_depth;

	$class_arr = [];

	$class_arr[] = "block__content";

	$class ? $class_arr[] = $class : '';

	$type ? $class_arr[] = get_section_type($type) : "";

	$showcase ? $class_arr[] = "showcase--$showcase" : "";

	$feed ? $class_arr[] = "block__feed feed--$feed" : "";

	$background ? $class_arr[] = "background--$background" : "";

	$classes = implode(" ", $class_arr);

	debug_to_console($classes);

	$output = "<section class=\"$classes\">".do_shortcode($content)."</section>";

	unset($class_arr);

	$section_depth--;

	return $output;

}

add_shortcode('section_sub_2', 'content');


function sub_content( $atts, $content = null ) {
	extract(shortcode_atts(array( "class" => "", "type" => "", "feed" => "", "background" => "" ), $atts));

	global $section_depth;

	++$section_depth;

	$class_arr = [];

	$class_arr[] = "section__content--sub";

	$class ? $class_arr[] = $class : '';

	$type ? $class_arr[] = get_section_type($type) : "";

	$feed ? $class_arr[] = "block__feed feed--$feed" : "";

	$background ? $class_arr[] = "background--$background" : "";

	$classes = implode(" ", $class_arr);

	$output = "<section class=\"$classes\">".do_shortcode($content)."</section>";

	$section_depth--;

	return $output;

}

add_shortcode('section_sub_3', 'sub_content');



function hanging( $atts, $content = null ) {

	extract(shortcode_atts(array( "class" => '' ), $atts));

	$output = '';

	$class ? " $class" : "";

	$output .= "<div class=\"container--hanging$class\">".do_shortcode(img_wpautop($content))."</section>";

}

add_shortcode('hanging', 'hanging');



/*********************
 FOOTERS
*********************/

function footer_section($atts, $content = null ) {
	extract(shortcode_atts(array( "class" => ""), $atts));

	global $section_depth;

	$class_arr = [];

	switch($section_depth) :

		case 1:

			$class_arr[] = 'section__footer';

		break;

		case 2:

			$class_arr[] = 'content__footer';

		break;

		default:

			$class_arr[] = 'block__footer';

	endswitch;

	$class ? $class_arr[] = trim($class) : '';

	$classes = implode(" ", $class_arr);

	return "<footer class=\"$classes\">".do_shortcode($content)."</footer>";

	unset($class_arr);

}

add_shortcode('footer', 'footer_section');


// BLOCKS


function block__banner( $atts, $content = null ) {

	extract(shortcode_atts(array( "class" => '', "color" => 'light grey' ), $atts));

	$class ? " $color $class" : " $color";

	add_shortcode("header", "content__header"); add_shortcode("title", "content__title");

	return "<section class=\"block--banner$class\">".do_shortcode($content)."</section>";

}

add_shortcode('banner', 'block__banner');



function block__feed( $atts, $content = null ) {

	extract(shortcode_atts(array( "class" => "", "post_type" => "design-proj", "num" => "3" ), $atts));

	$class = $class ? strpos($class, " ") === 0 ? $class : " $class" : "";

	add_shortcode("header", "content__header"); add_shortcode("title", "content__title");

	// return content

}

add_shortcode('feed', 'block__feed');



function block__img_content( $atts, $content = null ) {

	extract(shortcode_atts(array( "class" => '' ), $atts));

	$block__img = true;

	$class = $class ? strpos($class, " ") === 0 ? $class : " $class" : "";

	add_shortcode("header", "content__header"); add_shortcode("title", "content__title");

	return "<div class=\"block--img-content$class\">".do_shortcode($content)."</div>";

}

add_shortcode('img-content', 'block__img_content');



function block__definition( $atts, $content = null ) {

	extract(shortcode_atts(array( "class" => "", "term" => "", "type" => "" ), $atts));

	$class = $class ? strpos($class, " ") === 0 ? $class : " $class" : "";

	add_shortcode("header", "content__header"); add_shortcode("title", "content__title");

	$output = '';

	$arr_check = array('n' => 1, 'v' => 1, 'a' => 3, 'p' => 4);

	if ( $type && array_key_exists(substr($type, 0, 1), $arr_check) !== false) :

		$abbr = substr($type, 0, $arr_check[(substr($type, 0, 1))]);
		
	endif;

	if (isset($term)) :
		$output .= '<dl class="row--fluid block--definition">';
		$output .= '<dt class="flex__item--3 title _section definition__title">'.$term.'</dt>';
		if ( $type && $abbr ) :
			$output .= '<abbr class="flex__item--1 definition__info" title="'.$type.'">&#40;'.$abbr.'&#41;</abbr>';
		endif;
		$output .= '<dd class="flex__item--8 content__section definition__content">'.do_shortcode($content).'</dd>';
		$output .= '</dl>';
		else :
			break;
	endif;

	$allowed_tags = array(
		'dl' => array(
      'class' => array(),
    ),
    'dt' => array(
    	'class' => array(),
  	),
    'abbr' => array(
    	'class' => array(),
    	'title' => array(),
  	),
    'dd' => array(
    	'class' => array(),
  	),
	);
	
	return wp_kses($output, $allowed_tags);

}

add_shortcode('definition', 'block__definition');



function block__showcase( $atts, $content = null ) {

	extract(shortcode_atts(array( "class" => '' ), $atts));

	$class = $class ? strpos($class, " ") === 0 ? $class : " $class" : "";

	$output = '';

	$output .= "<section class=\"block--showcase$class\">".do_shortcode($content)."</section>";

}

add_shortcode('showcase', 'block__showcase');



function block__form( $atts, $content = null ) {

	extract(shortcode_atts(array( "class" => '' ), $atts));

	$class = $class ? strpos($class, " ") === 0 ? $class : " $class" : "";

	$output = '';

	$output .= "<section class=\"block--form$class\">".do_shortcode($content)."</section>";

}

add_shortcode('form', 'block__form');



function block__text( $atts, $content = null ) {

	extract(shortcode_atts(array( "class" => '' ), $atts));

	$class = $class ? strpos($class, " ") === 0 ? $class : " $class" : "";

	$output = '';

	$output .= "<section class=\"block--text$class\">".do_shortcode($content)."</section>";

}

add_shortcode('text', 'block__text');



?>