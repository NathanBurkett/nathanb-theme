<?php

// shortcodes to be removed. Usage: '$shortcode_tag',

$array_remove_sc = array(
    // 'gallery',
    // 'imbed',
    // 'audio',
    // 'video',
    'wp_caption'
    );

// shortcodes removing function

foreach ( $array_remove_sc as $remove_sc ) {
    remove_shortcode( $remove_sc, $remove_sc.'_shortcode' );
}

/************* SHORTCODES *****************/

// BLOCKQUOTE

function blockquote( $atts, $content = null ) {

    extract( shortcode_atts( array( 'author' => '', 'cite' => '' ), $atts ) );

    $output = '';

        $output .= '<div class="row block--quote">';

            $output .= '<blockquote' . ( !empty($cite) ? ' cite="' . $cite . '"' : '' ) . '>';

                $output .= '<p class="quote__content">' . do_shortcode( $content ) . '</p>';

                if (!empty($author)) :

                    $output .= '<' . ( !empty($cite) ? 'a' : 'p' ) . ' class="quote__author" ' . ( !empty($cite) ? 'href="' . $cite . '"' : '' ) .'>' . $author . '</' . ( !empty($cite) ? 'a' : 'p' ) . '>';

                endif;

            $output .= '</blockquote>';

        $output .= '</div>';

    return $output;

}

add_shortcode('blockquote', 'blockquote');




// DEFINITION

 function definition( $atts, $content = null) {

    extract(shortcode_atts(array( "term" => '', "type" => '' ), $atts));

    $output = '';

        $output = '<dl class="row block--definition">';

        if (!empty($term)) {

            $output .= '<dt class="definition__title">'. $term . '</dt>';

            if (!empty($type)) {

                $output .= '<span class="definition__info">&#40;' . $type . '&#41;</span>';

            }

        }

        $output .= '<dd class="definition__content">' . do_shortcode($content) . '</dd>';

        $output .= '</dl>';

    return $output;

}

add_shortcode('definition', 'definition');




// IMPORTANT LINK

 function important_link( $atts, $content = null) {

    extract(shortcode_atts(array( "text" => '' ), $atts));

    $output = '';

    return $output = '<a class="link--important" href="' .  $content . '" title="' . ( $text ?: '' ) . '>' . ( $text ?: $content ) . '</a>';

}

add_shortcode('important_link', 'important_link');




// DIVIDER

function horizontal_line( $atts) {

	extract( shortcode_atts( $atts ) );

  	return '<hr class="row">';

}

add_shortcode('divider', 'horizontal_line');




?>