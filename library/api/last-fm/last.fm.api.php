<?php

session_start();

$lastfm_user = "nathanburkett";
$lastfm_api = "af183bceffbe00ee9edf33d23e2111b3";
$format = "json";

function get_content_from_lastfm($url) {
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($ch, CURLOPT_USERAGENT, "nathanburkett");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$content = curl_exec($ch);
	curl_close($ch);
	return $content;
}

$tracks = get_content_from_lastfm("http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user={$lastfm_user}&api_key={$lastfm_api}&format={$format}");

if ($tracks) {
	$file = "last-fm.txt";
	$fh = fopen($file, 'wb') or die("can't open file");
	fwrite($fh, $tracks);
	fclose($fh);

	if (file_exists($file)) {
	   echo $file . " successfully written (" .round(filesize($file)/1024)."KB)";
	} else {
	    echo "Error encountered. File could not be written.";
	}
}

// echo "<pre>",print_r(json_decode($tracks, true)),"</pre>";

session_destroy();

?>