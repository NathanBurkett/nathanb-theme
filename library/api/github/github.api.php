<?php

session_start();

$github_user = "NathanBurkett";
$github_repo = "NathanB-theme";

function get_content_from_github($url) {
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($ch, CURLOPT_USERAGENT, "NathanBurkett");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$content = curl_exec($ch);
	curl_close($ch);
	return $content;
}

$repo = get_content_from_github("https://api.github.com/repos/{$github_user}/{$github_repo}/commits?sha=dev&per_page=100");

$commit_arr = json_decode($repo, true);
$construct = [];
date_default_timezone_set('America/Denver');
foreach ($commit_arr as $commit) :
	$id = $commit['sha'];
	$date = $commit['commit']['author']['date'];
	$time_ago = strtotime($date);
	$construct[] = array(
		'commit_id' => $id,
		'time_ago' => $time_ago,
		'date' => $date,
		'author' => $commit['commit']['author']['name'],
		'desc' => $commit['commit']['message'],
		'url' => $commit['html_url']
	);
endforeach;

// $e_cont = json_decode(file_get_contents('github.txt'), true);

// $master_arr = array_merge($construct, $e_cont);

// echo "<pre>",print_r($master_arr),"</pre>";

if ($construct) {
	$arr_file = "github.txt";
	$o_cons = fopen($arr_file, 'wb') or die("can't open file");
	fwrite($o_cons, json_encode($construct));
	fclose($o_cons);

	if (file_exists($arr_file)) {
	   echo $arr_file . " successfully written (" .round(filesize($arr_file)/1024)."KB)";
	} else {
	    echo "Error encountered. File could not be written.";
	}
}

// echo "<pre>",print_r(json_decode($repo, true)),"</pre>";

session_destroy();

?>