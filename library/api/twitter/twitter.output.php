<?php

$dir = dirname(__FILE__);

$tweets_json = file_get_contents($dir.'/twitter.txt');



if ($tweets_json) :

	date_default_timezone_set('America/Denver');

	$tweets = json_decode($tweets_json, true);

	$tweet = $tweets[0];

	$date = strtotime($tweet['created_at']);

	echo '<div class="social__item">';

	$username = '%(^|\s)@(\w+)%';
	$hashtag = '%(^|\s)#(\w+)%';
	$links = '%(https?:\/\/\S+)%';

	$status = $tweet['text'];
	$status = preg_replace($links, '<a href="$1" target="_blank">$1</a>', $status);
	$status = preg_replace($hashtag, '$1<a href="http://twitter.com/search?q=$1" target="_blank">#$2</a>', $status);
	$status = preg_replace($username, '$1<a href="http://twitter.com/$1" target="_blank">@$2</a>', $status);

		echo '<p class="feed__content">',$status,'</p>';
		echo '<p class="item__meta meta--time"><a href="https://twitter.com/nathan_burkett/status/',$tweet['id_str'],'" target="_blank">',human_time_diff($date),' ago</a></p>';

	echo '</div>';

endif;

?>