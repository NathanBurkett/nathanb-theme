<?php

$dir = dirname(__FILE__);

$imgs_json = file_get_contents($dir.'/instagram.txt');

if ($imgs_json) :

	$imgs = json_decode($imgs_json, true);

	$i = 0;

	$count_num = count($imgs);

	$limit = $count_num < 18 ? $count_num - ($count_num % 6) : 18;

	for ($i = 0; $i < $limit; $i++):

		$img = $imgs[$i];

	echo i % 6 === 0 ? '<div class="row--flex">' : '';

	echo '<li class="list__item gallery__item js--load__block social__item"><a class="item__link" href="',$img['link'],'">';
	echo '<img class="gallery__img" src ="',$img['images']['standard_resolution']['url'],'" alt="'+caption+' - Follow Nathan Burkett on Instagram">';
	echo caption != null ? '<p class="item__description">'+caption_text+'</p>' : '';
	echo '</a></li>';

	echo i % 6 === 5 ? '</div>' : '';

	$i++;

	endfor;

endif;

?>