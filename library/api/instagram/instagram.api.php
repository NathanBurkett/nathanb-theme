<?php

session_start();

$instagram_user = "nathanburkett";
$user_id = "345017403";
$client_id = "ea15d9cd95b54162bd8477e84e43bb7f";
$num = "16";


function get_instagram_feed($url){
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($ch, CURLOPT_USERAGENT, "345017403");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$content = curl_exec($ch);
	curl_close($ch);
	return $content;
}

$photos = get_instagram_feed("https://api.instagram.com/v1/users/{$user_id}/media/recent/?client_id={$client_id}&count={$num}");

if ($photos) {
	$file = "instagram.txt";
	$fh = fopen($file, 'wb') or die("can't open file");
	fwrite($fh, $photos);
	fclose($fh);

	if (file_exists($file)) {
	   echo $file . " successfully written (" .round(filesize($file)/1024)."KB)";
	} else {
	    echo "Error encountered. File could not be written.";
	}
}

// echo "<pre>",print_r(json_decode($photos, true)),"</pre>";

session_destroy();

?>