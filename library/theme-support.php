<?php
/**
 * THEME-SUPPORT.PHP
 * Theme supporting functions
 * Use for altering theme structure / head / attaching assets. Functions and outputting elements
 * go in functions.php or other sources.
 *
 * @author Roadwork Rah, LLC
 * @author Nathan Burkett <nathan@roadworkrah.com> (@nathan_burkett)
 *
 * @since 1.0.0
 *
 * @see library/theme-contents.txt  For a location of files and functions
 *
 * @since 0.2
 */

/**
 * THEME HEAD CLEANUP
 * Bulk grouping of theme filters and actions to cleanup the <head> element on the frontend
 *
 * @since 0.2
 */  

function rah_head_cleanup() {
	//** Category Feeds
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	//** Post And Comment Feeds
	remove_action( 'wp_head', 'feed_links', 2 );
	//** Edit URI Link
	remove_action( 'wp_head', 'rsd_link' );
	//** Windows Live Writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	//** Index Link
	remove_action( 'wp_head', 'index_rel_link' );
	//** Previous Link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	//** Start Link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	//** Links for Adjacent Posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	//** WP Version
	remove_action( 'wp_head', 'wp_generator' );
  //** Remove WP Version From CSS
  add_filter( 'style_loader_src', 'rah_remove_wp_ver_css_js', 9999 );
  //** Remove WP Version From Scripts
  add_filter( 'script_loader_src', 'rah_remove_wp_ver_css_js', 9999 );

}


/**
 * REWRITE PAGE TITLE
 * Rewrite the page title for a better output depending on the page
 *
 * @param string  $title  			Current page title
 * @param string  $sep  				Separator between elements in title
 * @param string  $seplocation  Right|Left -> Which side the Site name is on
 * @return Formatted title
 * @source http://www.deluxeblogtips.com/2012/03/better-title-meta-tag.html
 * @since 0.2
 */  

// http://www.deluxeblogtips.com/2012/03/better-title-meta-tag.html
function rw_title( $title, $sep, $seplocation ) {
  global $page, $paged;

  // Don't affect in feeds.
  if ( is_feed() ) return $title;

  // Add the blog's name
  if ( 'right' == $seplocation ) {
    $title .= get_bloginfo( 'name' );
  } else {
    $title = get_bloginfo( 'name' ) . $title;
  }

  // Add the blog description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );

  if ( $site_description && ( is_home() || is_front_page() ) ) {
    $title .= " {$sep} {$site_description}";
  }

  // Add a page number if necessary:
  if ( $paged >= 2 || $page >= 2 ) {
    $title .= " {$sep} " . sprintf( __( 'Page %s', 'dbt' ), max( $paged, $page ) );
  }

  return $title;

}


/**
 * REMOVE WP VERSION FROM RSS AND SCRIPTS
 * Remove the WP Version to prevent any malicious targets and/or attacks
 *
 * @since 0.2
 */ 

function rah_rss_version() { return ''; }

// remove WP version from scripts
function rah_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}


/**
 * REMOVE INJECTED STYLE FROM COMMENTS WIDGET
 * @since 0.2
 */

function rah_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

function rah_remove_recent_comments_style() {
	global $wp_widget_factory;
	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
	}
}

/**
 * REMOVE INJECTED STYLES FROM GALLERY
 * @since 0.2
 */ 
function rah_gallery_style($css) {
	return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}


/**
 * SCRIPTS & ENQUEUING
 * Necessary scripts and their enqueuing on certain pages
 *
 *
 * @since 0.2
 */ 

function rah_scripts_and_styles() {

	global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

  if (!is_admin()) {

    // modernizr (without media query polyfill)
    wp_register_script( 'modernizr', get_stylesheet_directory_uri() . '/library/js/libs/modernizr.custom.min.js', array(), '2.5.3', false );

    // register main stylesheet
    wp_register_style( 'main-stylesheet', get_stylesheet_directory_uri() . '/library/css/style.css', array(), '', 'all' );

    // styleguide stylesheet
    wp_register_style( 'styleguide-stylesheet', get_stylesheet_directory_uri() . '/library/css/style-guide.css', array(), '', 'all' );

    // ie-only style sheet
    wp_register_style( 'ie-only', get_stylesheet_directory_uri() . '/library/css/ie.css', array(), '' );

    // comment reply script for threaded comments
    if ( is_singular('post') AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }

    wp_register_script( 'head-js', get_stylesheet_directory_uri() . '/library/js/head.js', array( 'jquery' ), '', false );

    wp_register_script( 'foot-js', get_stylesheet_directory_uri() . '/library/js/foot.js', array( 'jquery' ), '', true );

    // Prism
    wp_register_script( 'prism', get_stylesheet_directory_uri() . '/library/js/prism.js', array(), '', false);

    // Masonry
    wp_register_script( 'masonry', '//cdn.jsdelivr.net/masonry/3.1.5/masonry.min.js', array('jquery'), '', true );

    // enqueue styles and scripts
    wp_enqueue_script( 'modernizr' );
    wp_enqueue_style( 'main-stylesheet' );
    wp_enqueue_style('ie-only');

    $wp_styles->add_data( 'ie-only', 'conditional', 'lt IE 9' ); // add conditional wrapper around ie stylesheet

    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'head-js' );
    wp_enqueue_script('foot-js');
    // wp_enqueue_script( 'sidr' );

	  if(get_query_var('is_styleguide')){
	  	wp_enqueue_style('styleguide-stylesheet');
	  	wp_enqueue_script( 'head-js' );
	  	wp_enqueue_script( 'foot-js' );
	  	wp_enqueue_script('prism');
	  }
  }
}


/**
 * SCRIPTS & ENQUEUING FOR WP ADMIN
 * Necessary scripts and their enqueuing on backend
 * @since 0.2
 */ 

add_action( 'admin_init', 'custom_post_admin_css' );

function custom_post_admin_css() {
	if (is_admin()) {
		wp_register_style( 'back-end', get_stylesheet_directory_uri() . '/library/css/back-end.css', array('cmb-styles'), '', 'all' );
		wp_register_script( 'admin-js', get_stylesheet_directory_uri() . '/library/js/libs/admin.js', array('jquery'), '', true);
		
		wp_enqueue_style( 'back-end' );
		wp_enqueue_script( 'admin-js' );
	}
}

/**
 * REMOVE WPCF7 JS AND STYLES FROM FRONT-END
 * Enqueue only on the templates using the styles / scripts
 *
 * @since 0.2
 */ 

add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

if ( is_page_template('page-contact.php') || is_single('page-new-project.php') ) {
  wp_register_style( 'contact-form-7', 100 );
  wp_register_script( 'contact-form-7', 100 );
}


/**
 * THEME SUPPORT
 * Add certain theme features and support
 *
 * @since 0.2
 */ 

//** Adding WP 3+ Functions & Theme Support
function rah_theme_support() {

//** WP Thumbnails Support (sizes handled in functions.php)
add_theme_support('post-thumbnails');

//** Set Default Thumb Size
set_post_thumbnail_size(125, 125, true);

//** Add RSS Supoort
add_theme_support('automatic-feed-links');

//** Add header image support -> Go here: http://themble.com/support/adding-header-background-image-support/

//** To add post format support -> Uncomment the formats

/*	add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	); */

//** WP Menus
	add_theme_support( 'menus' );

//** Registering WP 3.0+ Menus
	register_nav_menus(
		array(
			'main-nav' => __( 'Main Navigation', 'rah_theme' ),   // Main Nav In Header
			'footer-links' => __( 'Footer Navigation', 'rah_theme' ) // Footer Nav
		)
	);
}


/**
 * MAIN NAVIGATION MENU
 * Main theme header navigation
 * 
 * @see http://codex.wordpress.org/Function_Reference/wp_nav_menu for formatting the menu
 * @see library/nav-walker.php for custom menu output format
 * @since 0.2
 */

function rah_main_nav() {
    wp_nav_menu(array(
    	'container' => false,                       // Remove Nav Container
    	'container_class' => 'row',           			// Class of Container -- Optional
    	'menu' => 'Main Navigation',                // Nav Name
    	'menu_id' =>  '',														// Custom Nav ID -- Optional
    	'menu_class' => '',         								// Custom Nav Class -- Optional
    	'theme_location' => 'main-nav',             // Location in the Theme
    	'before' => '',                             // before the menu
        'after' => '',                            // after the menu
        'link_before' => '',                      // before each link
        'link_after' => '',                       // after each link
        'depth' => 0,                             // Limit Depth of nav
        'items_wrap' => '%3$s',
    	'fallback_cb' => 'rah_main_nav_fallback',		// Fallback Function
    	// 'walker' => new Clean_Walker_Nav
	));
}


/**
 * FOOTER NAVIGATION MENU
 * Main theme footer navigation
 * 
 * @see http://codex.wordpress.org/Function_Reference/wp_nav_menu for formatting the menu
 * @see library/nav-walker.php for custom menu output format
 * @since 0.2
 */ 

function rah_footer_links() {
    wp_nav_menu(array(
    	'container' => '',                              // remove nav container
    	'container_class' => 'row',   // class of container (should you choose to use it)
    	'menu' => 'Footer Navigation',                       // nav name
    	'menu_class' => 'nav__list nav--footer',      // adding custom nav class
    	'theme_location' => 'footer-links',             // where it's located in the theme
    	'before' => '',                                 // before the menu
        'after' => '',                                  // after the menu
        'link_before' => '',                            // before each link
        'link_after' => '',                             // after each link
        'depth' => 0,                                   // limit the depth of the nav
    	'fallback_cb' => 'rah_footer_links_fallback',  // fallback function
    	// 'walker' => new Clean_Walker_Nav
	));
}

//** Fallback for Header Menu
function rah_main_nav_fallback() {
	wp_page_menu( 'show_home=Home' );
}


?>
