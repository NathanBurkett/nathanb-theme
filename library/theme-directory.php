<?php
/**
 * THEME DIRECTORY
 * This is the main directory of contents in this theme
 * Wanted it in php to highlight and show colored syntax for better maintaining and readability
 *
 * @author Roadwork Rah, LLC
 * 				 Nathan Burkett <nathan@roadworkrah.com> (@nathan_burkett)
 *
 * @since 0.2
 *
 *
 */

/**
 * / library
 * |--/ ajax
 * |  |   >> Post type templates used in ajax requests
 * |  |
 * |  |-- design-project.php
 * |  |     >> Design project ajax template
 * |  |
 * |  |-- post.php
 * |  |     >> Post ajax template
 * |  |
 * |  |-- request.php
 * |  ^     >> Generic ajax template that grabs the template type of the post type it's filtering
 * |
 * |--/ code-snippet
 * |  |   >> Code snippet custom post type
 * |  |
 * |  |-- code-snippet.php
 * |  ^     >> Custom post type code
 * |
 * |--/ css
 * |  |   >> All css output by sass via grunt.
 * |  |
 * |  |-- back-end.css back-end.css.map
 * |  |     >> Output from back-end.scss in /scss folder
 * |  |
 * |  |-- login.css && login.css.map
 * |  |     >> Output from login.scss in /scss folder
 * |  |
 * |  |-- style-guide.css && style-guide.css.map
 * |  |     >> Output from style-guide.scss in /scss folder
 * |  |
 * |  |-- style.css && style.css.map
 * |  ^     >> Output from style.scss in /scss folder
 * |
 * |--/ custom-post
 * |  |   >> Blank template for custom post type... Duplicate and edit
 * |  |
 * |  |-- custom-post-type.php
 * |  ^		  >> Custom post type code
 * |
 * |--/ design-project
 * |  |   >> Design project custom post type
 * |  |
 * |  |-- design-project.php
 * |  ^     >> Design project post type code
 * |				    ** This post type just uses normal cats and tags -> relatable and searchable with
 * |						   regular posts and other site content
 * |
 * |--/ dribbble
 * |  ^   >> Dribbble cron tasks write here
 * |
 * |--/ font-include
 * |  |   >> Font include custom post type
 * |  |
 * |  |-- font-include.php
 * |  ^     >> Font include post type code
 * |
 * |--/ images
 * |  |
 * |  |--/ favicon
 * |  |  |
 * |  |  ^
 * |  |
 * |  |--/ styleguide
 * |  |  |
 * |  |  ^
 * |  |
 * |  |--/ svg
 * |  |  |
 * |  |  ^
 * |  |
 * |  |-- custom-post-icon.png
 * |  |
 * |  |-- icons.svg
 * |  |
 * |  |-- login-logo.png
 * |  |
 * |  |-- *logo(s)
 * |  |
 * |  |-- nothing.gif
 * |  ^
 * |--/ js
 * |  |
 * |  |--/ functions
 * |  |  |
 * |  |  ^
 * |  |
 * |  |--/ libs
 * |  |  |
 * |  |  ^
 * |  |
 * |  |-- foot.js && foot.min.js
 * |  |
 * |  |
 * |  |
 * |  |-- head.js && head.min.js
 * |  |
 * |  |
 * |  |
 * |  |-- prism.js && prism.min.js
 * |  |
 * |  |
 * |  |
 * |  |-- TinyMCE_js.js
 * |  ^
 * |
 * |--/ scss
 * |  |
 * |  |--/ 01-generic
 * |  |  |
 * |  |  |-- _00-functions.scss
 * |  |  |
 * |  |  |-- _01-variables.scss
 * |  |  |
 * |  |  |-- _02-mixins.scss
 * |  |  |
 * |  |  |-- _03-reset.scss
 * |  |  |
 * |  |  |-- _04-placeholders&silents.scss
 * |  |  |
 * |  |  |-- _05-font-face.scss
 * |  |  |
 * |  |  |-- _06-rows.scss
 * |  |  ^
 * |  |
 * |  |--/ 02-base
 * |  |  |
 * |  |  |-- _01-global-classes.scss
 * |  |  |
 * |  |  |-- _02-main.scss
 * |  |  |
 * |  |  |-- _03-links.scss
 * |  |  |
 * |  |  |-- _04-headings.scss
 * |  |  |
 * |  |  |-- _05-text.scss
 * |  |  |
 * |  |  |-- _06-lists.scss
 * |  |  |
 * |  |  |-- _07-media.scss
 * |  |  |
 * |  |  |-- _08-forms.scss
 * |  |  |
 * |  |  |-- _09-tables.scss
 * |  |  |
 * |  |  |-- _10-animation.scss
 * |  |  |
 * |  |  |-- _11-code.scss
 * |  |  ^
 * |  |
 * |  |--/ 03-objects
 * |  |  |
 * |  |  |-- _01-layout.scss
 * |  |  |
 * |  |  |-- _02-header.scss
 * |  |  |
 * |  |  |-- _03-nav.scss
 * |  |  |
 * |  |  |-- _04-main.scss
 * |  |  |
 * |  |  |-- _05-footer.scss
 * |  |  |
 * |  |  |-- _06-text.scss
 * |  |  |
 * |  |  |-- _07-icons.scss
 * |  |  |
 * |  |  |-- _08-buttons.scss
 * |  |  |
 * |  |  |-- _09-blocks.scss
 * |  |  |
 * |  |  |-- _11-tooltip.scss
 * |  |  |
 * |  |  |-- _12-accordion.scss
 * |  |  |
 * |  |  |-- _13-tabs.scss
 * |  |  |
 * |  |  |-- _14-sections.scss
 * |  |  |
 * |  |  |-- _15-article.scss
 * |  |  |
 * |  |  |-- _16-carousels.scss
 * |  |  |
 * |  |  |-- _17-comments.scss
 * |  |  |
 * |  |  |-- _18-post.scss
 * |  |  ^
 * |  |
 * |  |--/ 04-other
 * |  |  |
 * |  |  |-- _back-end.scss
 * |  |  |
 * |  |  |-- _no-js.scss
 * |  |  |
 * |  |  |-- _style-guide.scss
 * |  |  ^
 * |  |
 * |  |-- back-end.scss
 * |  |
 * |  |-- config.rb
 * |  |
 * |  |-- login.scss
 * |  |
 * |  |-- prism.css
 * |  |
 * |  |-- style-guide.scss
 * |  |
 * |  |-- style.scss
 * |  ^
 * |
 * |--/ shortcodes
 * |  |
 * |  |-- shortcodes--sections.php
 * |  |
 * |  |-- shortcodes.php
 * |  ^
 * |
 * |--/ style-guide
 * |  |
 * |  |--/ atom
 * |  |
 * |  |--/ entities
 * |  |
 * |  |--/ molecule
 * |  |
 * |  |--/ organism
 * |  |
 * |  |--/ scaffold
 * |  |
 * |  |-- helper-variables.php
 * |  |
 * |  |-- pangrams.php
 * |  |
 * |  |-- style-guide-functions.php
 * |  |
 * |  |-- styleguide.php
 * |  ^
 * |
 * |--/ translation
 * |  |
 * |  ^
 * |
 * |--/ twitteroauth
 * |  |
 * |  |-- OAuth.php
 * |  |
 * |  |-- twitteroauth.php
 * |  ^
 * |
 * / scaffold
 * |
 * |--/ content
 * |  |
 * |  |--/ custom_post
 * |  |  |
 * |  |  |-- archive&search.php
 * |  |  |
 * |  |  |-- content.php
 * |  |  |
 * |  |  |-- feed.php
 * |  |  ^
 * |  |
 * |  |--/ design-project
 * |  |  |
 * |  |  |--/ scaffold
 * |  |  |  |
 * |  |  |  |-- end-feed.php
 * |  |  |  |
 * |  |  |  |-- start-feed.php
 * |  |  |  ^
 * |  |  |
 * |  |  |-- ajax.php
 * |  |  |
 * |  |  |-- archive&search.php
 * |  |  |
 * |  |  |-- content.php
 * |  |  |
 * |  |  |-- feed--large.php
 * |  |  |
 * |  |  |-- feed.php
 * |  |  |
 * |  |  |-- footer.php
 * |  |  |
 * |  |  |-- header.php
 * |  |  ^
 * |  |
 * |  |--/ page
 * |  |  |
 * |  |  |--/ scaffold
 * |  |  |  |
 * |  |  |  |-- content.php
 * |  |  |  |
 * |  |  |  |-- footer.php
 * |  |  |  |
 * |  |  |  |-- header-inform.php
 * |  |  |  |
 * |  |  |  |-- header-main.php
 * |  |  |  |
 * |  |  |  |-- header-showcase.php
 * |  |  |  |
 * |  |  |  |-- header.php
 * |  |  |  |
 * |  |  |  |-- main-content.php
 * |  |  |  ^
 * |  |  |
 * |  |  |--/ templates
 * |  |  |  |
 * |  |  |  |-- inform.php
 * |  |  |  |
 * |  |  |  |-- page.php
 * |  |  |  |
 * |  |  |  |-- showcase.php
 * |  |  |  |
 * |  |  |  |-- template.php
 * |  |  |  ^
 * |  |  |
 * |  |  |-- archive&search.php
 * |  |  |
 * |  |  |-- feed.php
 * |  |  ^
 * |  |
 * |  |--/ post
 * |  |  |
 * |  |  |--/ scaffold
 * |  |  |  |
 * |  |  |  |-- end-feed.php
 * |  |  |  |
 * |  |  |  |-- start-feed.php
 * |  |  |  ^
 * |  |  |
 * |  |  |-- ajax.php
 * |  |  |
 * |  |  |-- archive&search.php
 * |  |  |
 * |  |  |-- content.php
 * |  |  |
 * |  |  |-- feed--large.php
 * |  |  |
 * |  |  |-- feed-text_only.php
 * |  |  |
 * |  |  |-- feed.php
 * |  |  ^
 * |  |
 * |  |--/ sections
 * |  |
 * |  |-- missing.php
 * |  ^
 * |
 * |--/ template
 * |  |
 * |  |-- standard-end.php
 * |  |
 * |  |-- standard-start.php
 * |  ^
 * |
 * |-- 404.php
 * |
 * |-- archive-custom_type.php
 * |
 * |-- archive-design-project.php
 * |
 * |-- archive.php
 * |
 * |-- comments.php
 * |
 * |-- favicon.ico
 * |
 * |-- footer-thin.php
 * |
 * |-- footer.php
 * |
 * |-- footer.php
 * |
 * |-- functions.php
 * |
 * |-- Gruntfile.js
 * |
 * |-- header-thin.php
 * |
 * |-- header.php
 * |
 * |-- index.php
 * |
 * |-- package.json
 * |
 * |-- page-contact.php
 * |
 * |-- page-home.php
 * |
 * |-- page-inform.php
 * |
 * |-- page-new-project.php
 * |
 * |-- page-showcase.php
 * |
 * |-- page-top_level-inform.php
 * |
 * |-- page-top_level-show.php
 * |
 * |-- page-what-we-do.php
 * |
 * |-- page.php
 * |
 * |-- README.rdoc
 * |
 * |-- screenshot.png
 * |
 * |-- search.php
 * |
 * |-- searchform.php
 * |
 * |-- sidebar.php
 * |
 * |-- single-custom_type.php
 * |
 * |-- single-design-project.php
 * |
 * |-- single.php
 * |
 * |-- style-guide.php
 * |
 * |-- style.css
 * |
 * |-- taxonomy-custom_cat.php
 * |
 * |-- template.php
 */

?>