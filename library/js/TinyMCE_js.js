(function() {


    tinymce.create('tinymce.plugins.Shortcodes', {

        init : function(ed, url) {
        },
        createControl : function(n, cm) {

            if(n=='Shortcodes'){
                var mlb = cm.createListBox('Shortcodes', {
                     title : 'Shortcodes',
                     onselect : function(v) {
                        if(tinyMCE.activeEditor.selection.getContent() == ''){
                            tinyMCE.activeEditor.selection.setContent( v )
                        }


                        if(v == 'shortcode 1'){

                            selected = tinyMCE.activeEditor.selection.getContent();

                            if( selected ){
                                //If text is selected when button is clicked
                                //Wrap shortcode around it.
                                content =  '[thirdwidth]'+selected+'[/thirdwidth]';
                            }else{
                                content =  '[thirdwidth][/thirdwidth]';
                            }

                            tinymce.execCommand('mceInsertContent', false, content);

                        }

                        if(v == 'shortcode 2'){

                            selected = tinyMCE.activeEditor.selection.getContent();

                            if( selected ){
                                //If text is selected when button is clicked
                                //Wrap shortcode around it.
                                content =  '[12]'+selected+'[/12]';
                            }else{
                                content =  '[12][/12]';
                            }

                            tinymce.execCommand('mceInsertContent', false, content);

                        }


                     }
                });


                // Add some menu items
                var my_shortcodes = ['shortcode 1','shortcode 2'];

                for(var i in my_shortcodes)
                    mlb.add(my_shortcodes[i],my_shortcodes[i]);

                return mlb;
            }
            return null;
        }


    });
    tinymce.PluginManager.add('Shortcodes', tinymce.plugins.Shortcodes);
})();