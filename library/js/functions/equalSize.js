(function($) {
       $.fn.equalSize = function(orient, breakPoint) {

           var items = $(this),
           dimension = ( orient == 'width' ) ? outerWidth() : ( orient == 'height' ) ? outerHeight() : return console.log('Orientation not valid!');

           // Bind functionality to appropriate events
           $(window).bind('load orientationchange smartresize', function() {
               biggest = 0;
               items.each(function() {
                   $(this).dimension('auto');
                   if($(this).dimension() > biggest) {
                       biggest = $(this).dimension();
                   }
               });

               var e = window;
                a = 'inner';
                if (!('innerWidth' in window )) {
                    a = 'client';
                    e = document.documentElement || document.body;
                }
                width = e[ a+'Width' ];

                // Equalize column heights if above the specified breakpoint
                if ( width >= breakPoint ) {
                    return items.each(function() {
                        $(this).dimension(biggest);
                    });
                }
           });
       }

   })(jQuery);

   // jQuery('div').equalSize(width, 768);