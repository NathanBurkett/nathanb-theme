var deBouncer = function($,cf,of, interval){
    // deBouncer by hnldesign.nl
    // based on code by Paul Irish and the original debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
        var timeout;
        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            }
            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);
            timeout = setTimeout(delayed, threshold || interval);
        };
    };
    jQuery.fn[cf] = function(fn){  return fn ? this.bind(of, debounce(fn)) : this.trigger(cf); };
};

//register debouncing functions
//deBouncer(jQuery,'new eventname', 'original event', timeout);
//Note: keep the jQuery namespace in mind, don't overwrite existing functions!

deBouncer(jQuery,'smartresize', 'resize', 100);
deBouncer(jQuery,'smartscroll', 'scroll', 100);
deBouncer(jQuery,'smartmousemove', 'mousemove', 100);

// jQuery(window).smartResize(function(e){
//     // do stuff as soon as the user stops resizing his browser for longer than 100ms
// });

// jQuery(window).smartScroll(function(e){
//     // do stuff as soon as the user stops scrolling for longer than 100ms
// });

// jQuery(window).smartMouseMove(function(e){
//     // do stuff as soon as the user stops moving his mouse for longer than 100ms
// });