(function($) {
    $.fn.seqFade = function(fade, delay) {
        var elements = this,
            l = elements.length,
            i = 0,
            f = (fade != '') ? fade : 400,
            d = (delay != '') ? delay : 4000;

        function execute() {
            var current = $(elements[i]);
            i = (i + 1) % l;

            current
               .fadeIn(f)
               .delay(d)
               .fadeOut(f, execute);
        }
        execute();
        return this;
    } ;
}(jQuery));

// jQuery('div').seqFade();