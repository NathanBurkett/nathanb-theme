(function($) {
  $.fn.equalWidth = function(breakPoint) {
    var items = this;

    // Bind functionality to appropriate events
      $(window).bind('load orientationchange smartresize', function() {
          widest = 0;
          items.each(function() {
              $(this).outerWidth('auto');
              if($(this).outerWidth() > widest) {
                  widest = $(this).outerWidth();
              }
          });

          var e = window;
           a = 'inner';
           if (!('innerWidth' in window )) {
               a = 'client';
               e = document.documentElement || document.body;
           }
           width = e[ a+'Width' ];

           // Equalize column widths if above the specified breakpoint
           if ( width >= breakPoint ) {
               return items.each(function() {
                   $(this).outerWidth(widest);
               });
           }
      });
  }

})(jQuery);

// jQuery('div').equalHeight(width, 768);
