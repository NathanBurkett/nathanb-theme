(function($) {
   $.fn.equalHeight = function(breakPoint) {

       var items = $(this);

       // Bind functionality to appropriate events
       $(window).bind('load orientationchange smartresize', function() {
           tallest = 0;
           items.each(function() {
               $(this).outerHeight('auto');
               if($(this).outerHeight() > tallest) {
                   tallest = $(this).outerHeight();
               }
           });

           var e = window;
            a = 'inner';
            if (!('innerWidth' in window )) {
                a = 'client';
                e = document.documentElement || document.body;
            }
            width = e[ a+'Width' ];

            // Equalize column heights if above the specified breakpoint
            if ( width >= breakPoint ) {
                return items.each(function() {
                    $(this).outerHeight(tallest);
                });
            }
       });
   }

})(jQuery);

// jQuery('div').equalHeight(width, 768);