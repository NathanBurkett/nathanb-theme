 (function($) {
 	jQuery('a[href^="mailto:"]').each(function() {
	  this.href = this.href.replace('[at]', '@').replace(/\[dot\]/g, '.');
	  this.innerHTML = this.href.replace('mailto:', '');
 	});
 })(jQuery);
 