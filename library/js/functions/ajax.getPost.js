// ajaxLoop.js

// a - post type
// b - number of posts
// c - change dir

(function( $ ){

    $.fn.ajaxGetPosts = function( a, b, c ) {

        var t = this;

        var loc = c === 'true' ? c : a;

        var d = window.location.protocol + "//" + window.location.host + "/wp-content/themes/roadworkrah/library/ajax/request.php";
        var err_msg = "<p class=\"item-status--error\">This content couldn't be loaded. Please try again later or email us here <a href=\"mailto:hello@roadworkrah.com\">Hello@RoadworkRah.com<a></p>";

        var load_posts = function() {
            $.ajax({
                type : "GET",
                data : {numPosts: b, postType: a},
                dataType : "html",
                url : d,

                success : function(data){
                    $data = $(data);
                    if($data.length){
                        $data.hide();
                        t.append($data);
                        $data.fadeIn(500);
                    } else {
                        t.append(err_msg);
                    }
                },
                error : function(jqXHR, textStatus, errorThrown){
                    t.append(err_msg);
                    console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }

            });
        }
        load_posts();
    }
}( jQuery ));