function relative_time(tdate) {

      var system_date = new Date(Date.parse(tdate));
      var user_date = new Date();

      if (K.ie) {
          system_date = Date.parse(tdate.replace(/( \+)/, ' UTC$1'))
      }

      var diff = Math.floor((user_date - system_date) / 1000);
      if (diff <= 1) {return "just now";}
      if (diff < 20) {return diff + " seconds ago";}
      if (diff < 40) {return "half a minute ago";}
      if (diff < 60) {return "less than a minute ago";}
      if (diff <= 90) {return "one minute ago";}
      if (diff <= 3540) {return Math.round(diff / 60) + " minutes ago";}
      if (diff <= 5400) {return "1 hour ago";}
      if (diff <= 86400) {return Math.round(diff / 3600) + " hours ago";}
      if (diff <= 129600) {return "1 day ago";}
      if (diff < 604800) {return Math.round(diff / 86400) + " days ago";}
      if (diff <= 1123200) {return "1 week ago";}
            if (diff <= 1728000) {return "2 weeks ago";}
            if (diff <= 2332800) {return "3 weeks ago";}
            if (diff <= 5097600) {return "1 month ago";}
            if (diff <= 34214400) {return Math.round(diff / 3110400) + " months ago";}
      return "last year";

    }

    // from http://widgets.twimg.com/j/1/widget.js
    var K = function () {
      var a = navigator.userAgent;
      return {
        ie: a.match(/MSIE\s([^;]*)/)
        }
    }();
(function( $ ){

  $('body').on('click touchstart', function(e) {
    e.preventDefault();
    if ($('.js--modal').is(e.target)) {
      var d = window.location.protocol + "//" + window.location.host + "/wp-content/themes/nathanb/library/ajax/commits.php";
      var err_msg = "<p class=\"item-status--error\">This content couldn't be loaded. View the commit history here: <a href=\"https://github.com/NathanBurkett/nathanb-theme/\">https://github.com/NathanBurkett/nathanb-theme/<a></p>";
          $.ajax({
              type : "GET",
              // data : {numPosts: b, postType: a},
              dataType : "html",
              url : d,

              success : function(data){
                  $data = $(data);
                  var cont = "<div class='container--modal'></div>";
                  if($data.length){
                    $data.hide();
                    $('#container').addClass('modal--visible');
                    $(cont).appendTo("#container").hide().fadeIn(250, function() {
                      $data.appendTo(this).fadeIn(250);
                      $('.block--modal .social__item').each(function(i) {
                        $(this).css({'opacity': 0, 'margin-top' : '-0.75em'});
                        $(this).delay((i++) * 100).animate({'opacity': '1', 'margin-top' : 0}, 250, function() {
                          $(this).removeAttr("style");
                        });
                      });
                    });

                  } else {
                      t.append(err_msg);
                  }
              },
              error : function(jqXHR, textStatus, errorThrown){
                  t.append(err_msg);
                  console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
              }
          });
      }

      if (($('#container').hasClass('modal--visible')) && ((!$(".block--modal").is(e.target)) || ($(".modal__close").is(e.target)))) {
        $(".block--modal").fadeOut(250, function() {
          $(this).remove();
          $(".container--modal").fadeOut(250, function() {
            $(this).remove();
          });
        });
      }

  });

}( jQuery ));
(function($) {

	var themename = "nathanb",
	// other vars
	target = $('#js--github'),
	limit = 1,
	dirBuild = window.location.protocol + "//" + window.location.host + "/";

	// GET /repos/:owner/:repo/stats/contributors to get total num of commits

	function sformat(s) {
		var overall = '',
    		fm = [
          Math.floor(s / 60 / 60 / 24), // DAYS
          Math.floor(s / 60 / 60) % 24 // HOURS
    		];
    overall += fm[0] > 1 ? fm[0] + ' days and ' : fm[0] + ' day and ';
    overall += fm[1] + ' hours';
    return overall;
	}


	$.getJSON(dirBuild+'wp-content/themes/'+themename+'/library/api/github/github.txt',
		function(feed) {
			var feedHTML = '',
					feed_len = feed.length,
					earliest = feed[feed_len - 1]['time_ago'];
					latest = feed[0]['time_ago'];
					output = '',
					total_time = sformat(latest - earliest),
					count = 1;
					$.each(feed, function() {
						if ( count > limit) { return false; }
						var t = this,
								commit = t.commit_id,
								time = t.time_ago,
								message = t.desc,
								url = t.url;

								commit = $.trim(commit).substring(0, 10);
								time = relative_time(time);

						feedHTML += '<div class="social__item">';
						feedHTML += '<svg class="icon icon--github"><use xlink:href="http://dev.nathanb.me/wp-content/themes/nathanb/library/images/icons.svg#icon_github"></use></svg>';
						feedHTML += '<p class="item__title">'+message+'</p>';
						feedHTML += '<a class="item__link" href="'+url+'">'+commit+'</a><p class="item__meta meta--time">'+time+'</p></div>';
					count++;
			});
			output += '<p>This one-page website was build over a course of <strong>'+total_time+'</strong> and took <strong>'+feed_len+' git commits</strong>. Here is the latest one:';
			output += feedHTML;

	target.html(output);
	});
})(jQuery);
(function($) {

	var theme = "nathanb",
			profile = "nathan_burkett",
			displaylimit = 1,
			showdirecttweets = false,
			showretweets = false,
			showtweetlinks = false,
			showprofilepic = false,
			target = $('#js--twitter'),
			dirBuild = window.location.protocol + "//" + window.location.host + "/";

	$.getJSON(dirBuild+'wp-content/themes/'+theme+'/library/api/twitter/twitter.txt',
		function(feed) {
			var feedHTML = '',
			count = 1;
			for (var i = 0; i < feed.length; i++) {
				var tweetscreenname = feed[i].user.name,
						tweetusername = feed[i].user.screen_name,
						profileimage = feed[i].user.profile_image_url_https,
						status = feed[i].text,
						isaretweet = false,
						isdirect = false,
						tweetid = feed[i].id_str;

				status = status.replace(/(http[s]\:\/\/[A-Za-z0-9\/\.\?\=\-]*)/g,'<a href="$1" target="_blank">$1</a>');
            status = status.replace(/@([A-Za-z0-9\/_]*)/g,'<a href="http://twitter.com/$1" target="_blank">@$1</a>');
            status = status.replace(/#([A-Za-z0-9\/\.]*)/g,'<a href="http://twitter.com/search?q=$1" target="_blank">#$1</a>');

            // //If the tweet has been retweeted, get the profile pic of the tweeter
            // if(typeof feed[i].retweeted_status != 'undefined'){
            //    profileimage = feed[i].retweeted_status.user.profile_image_url_https;
            //    tweetscreenname = feed[i].retweeted_status.user.name;
            //    tweetusername = feed[i].retweeted_status.user.screen_name;
            //    tweetid = feed[i].retweeted_status.id_str
            //    isaretweet = true;
            //  };

             //Check to see if the tweet is a direct message
             // if (feed[i].text.substr(0,1) == "@") {
             //     isdirect = true;
             // }

            //console.log(feed[i]);

                if ((feed[i].text.length > 1) && (count <= displaylimit)) {
                    if (showtweetlinks == true) {
                        status = addlinks(status);
                    }

                    feedHTML += '<div class="social__item">';
                    feedHTML += '<p class="feed__content">'+status+'</p>';
                    feedHTML += '<a href="https://twitter.com/'+tweetusername+'/status/'+tweetid+'" class="meta__time" target="_blank">'+relative_time(feed[i].created_at)+'</a>';
                    feedHTML += '</div>';
                    count++;
                }
        }

	target.html(feedHTML);
			});
})(jQuery);
(function($) {

	var themename = "nathanb",
	// other vars
	target = $('#js--instagram'),
	limit = 36,
	dirBuild = window.location.protocol + "//" + window.location.host + "/";

	$.getJSON(dirBuild+'wp-content/themes/'+themename+'/library/api/instagram/instagram.txt',
		function(feed) {
			var data = feed['data'],
					total_num = data.length,
					feedHTML = '',
					count = 1;

					if (total_num < limit) {
						limit = total_num - (total_num % 6);
					}

			for (var i = 0; i < limit; i++) {
				var caption = '',
						img_url = data[i].images.standard_resolution.url,
						caption = data[i].caption,
						caption_text = caption != null ? caption.text : '';
						date = data[i].created_time,
						url = data[i].link,
						likes = data[i].likes.count;
				feedHTML += i % 6 === 0 ? '<div class="row--flex">' : '';

				feedHTML += '<li class="list__item gallery__item js--load__block social__item"><a class="item__link" href="'+url+'">';
				feedHTML += '<img class="gallery__img" src ="'+img_url+'" alt="'+caption+' - Follow Nathan Burkett on Instagram">';
				// feedHTML += '<p class="item__meta meta--likes">'+likes+'</p>';
				feedHTML += caption != null ? '<p class="item__description">'+caption_text+'</p>' : '';
				// feedHTML += '<p class="item__meta meta--time">'+date+'</p>';
				feedHTML += '</a></li>';

				feedHTML += i % 6 === 5 ? '</div>' : '';

			}

	target.html(feedHTML);

		});


})(jQuery);
(function($) {

	var themename = "nathanb",
	// other vars
	target = $('#js--last-fm'),
	limit = 1,
	dirBuild = window.location.protocol + "//" + window.location.host + "/";

	$.getJSON(dirBuild+'wp-content/themes/'+themename+'/library/api/last-fm/last-fm.txt',
		function(feed) {
			var feedHTML = '',
					tracks = feed['recenttracks']['track'],
					count = 1;
			for (var i = 0; i < limit; i++) {
				var artist = tracks[i].artist['#text'],
						// title = tracks[i].name,
						album = tracks[i].album['#text'];

				feedHTML += '';

				feedHTML += '<p class="item__title">'+artist+'</p>';
				feedHTML += '<p>'+album+'</p>';

			}
	target.html(feedHTML);
		});
})(jQuery);