// get element by class // radio && checkbox
// watch for change.
// if is:checked -> change path to icon_check--checked

(function($) {
	$.fn.checkboxChange = function(item, empty, full, remove_others) { // we'll get the current path while in the fn
		$(this).on("change", item, function() {
			var t = $(this);
			var icon = t.next().children(".icon").children("use");
			var loc = icon.attr("xlink:href");
			var n_loc = loc.split("#");
			var type = empty;
			if(t.is(':checked')) {
				type = full;
     	}
      icon.attr("xlink:href", n_loc[0] + "#" + type);
      if (remove_others) {
				t.parent(".form__item").siblings().children(".item__label").children(".icon").children("use").attr("xlink:href", n_loc[0] + "#" + empty);
			}
		});
	};
})(jQuery);

jQuery('.form__group').checkboxChange('.form__checkbox', 'icon_check--empty', 'icon_check--checked');

jQuery('.form__group').checkboxChange('.form__radio', 'icon_check--empty', 'icon_check--checked', true);