(function($) {

	var themename = "nathanb",
	// other vars
	target = $('#js--github'),
	limit = 1,
	dirBuild = window.location.protocol + "//" + window.location.host + "/";

	// GET /repos/:owner/:repo/stats/contributors to get total num of commits

	function sformat(s) {
		var overall = '',
    		fm = [
          Math.floor(s / 60 / 60 / 24), // DAYS
          Math.floor(s / 60 / 60) % 24 // HOURS
    		];
    overall += fm[0] > 1 ? fm[0] + ' days and ' : fm[0] + ' day and ';
    overall += fm[1] + ' hours';
    return overall;
	}


	$.getJSON(dirBuild+'wp-content/themes/'+themename+'/library/api/github/github.txt',
		function(feed) {
			var feedHTML = '',
					feed_len = feed.length,
					earliest = feed[feed_len - 1]['time_ago'];
					latest = feed[0]['time_ago'];
					output = '',
					total_time = sformat(latest - earliest),
					count = 1;
					$.each(feed, function() {
						if ( count > limit) { return false; }
						var t = this,
								commit = t.commit_id,
								time = t.time_ago,
								message = t.desc,
								url = t.url;

								commit = $.trim(commit).substring(0, 10);
								time = relative_time(time);

						feedHTML += '<div class="social__item">';
						feedHTML += '<svg class="icon icon--github"><use xlink:href="http://dev.nathanb.me/wp-content/themes/nathanb/library/images/icons.svg#icon_github"></use></svg>';
						feedHTML += '<p class="item__title">'+message+'</p>';
						feedHTML += '<a class="item__link" href="'+url+'">'+commit+'</a><p class="item__meta meta--time">'+time+'</p></div>';
					count++;
			});
			output += '<p>This one-page website was build over a course of <strong>'+total_time+'</strong> and took <strong>'+feed_len+' git commits</strong>. Here is the latest one:';
			output += feedHTML;

	target.html(output);
	});
})(jQuery);