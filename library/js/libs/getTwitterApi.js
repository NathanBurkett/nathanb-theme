(function($) {

	var theme = "nathanb",
			profile = "nathan_burkett",
			displaylimit = 1,
			showdirecttweets = false,
			showretweets = false,
			showtweetlinks = false,
			showprofilepic = false,
			target = $('#js--twitter'),
			dirBuild = window.location.protocol + "//" + window.location.host + "/";

	$.getJSON(dirBuild+'wp-content/themes/'+theme+'/library/api/twitter/twitter.txt',
		function(feed) {
			var feedHTML = '',
			count = 1;
			for (var i = 0; i < feed.length; i++) {
				var tweetscreenname = feed[i].user.name,
						tweetusername = feed[i].user.screen_name,
						profileimage = feed[i].user.profile_image_url_https,
						status = feed[i].text,
						isaretweet = false,
						isdirect = false,
						tweetid = feed[i].id_str;

				status = status.replace(/(http[s]\:\/\/[A-Za-z0-9\/\.\?\=\-]*)/g,'<a href="$1" target="_blank">$1</a>');
            status = status.replace(/@([A-Za-z0-9\/_]*)/g,'<a href="http://twitter.com/$1" target="_blank">@$1</a>');
            status = status.replace(/#([A-Za-z0-9\/\.]*)/g,'<a href="http://twitter.com/search?q=$1" target="_blank">#$1</a>');

            // //If the tweet has been retweeted, get the profile pic of the tweeter
            // if(typeof feed[i].retweeted_status != 'undefined'){
            //    profileimage = feed[i].retweeted_status.user.profile_image_url_https;
            //    tweetscreenname = feed[i].retweeted_status.user.name;
            //    tweetusername = feed[i].retweeted_status.user.screen_name;
            //    tweetid = feed[i].retweeted_status.id_str
            //    isaretweet = true;
            //  };

             //Check to see if the tweet is a direct message
             // if (feed[i].text.substr(0,1) == "@") {
             //     isdirect = true;
             // }

            //console.log(feed[i]);

                if ((feed[i].text.length > 1) && (count <= displaylimit)) {
                    if (showtweetlinks == true) {
                        status = addlinks(status);
                    }

                    feedHTML += '<div class="social__item">';
                    feedHTML += '<p class="feed__content">'+status+'</p>';
                    feedHTML += '<a href="https://twitter.com/'+tweetusername+'/status/'+tweetid+'" class="meta__time" target="_blank">'+relative_time(feed[i].created_at)+'</a>';
                    feedHTML += '</div>';
                    count++;
                }
        }

	target.html(feedHTML);
			});
})(jQuery);