(function($) {

	var themename = "nathanb",
	// other vars
	target = $('#js--last-fm'),
	limit = 1,
	dirBuild = window.location.protocol + "//" + window.location.host + "/";

	$.getJSON(dirBuild+'wp-content/themes/'+themename+'/library/api/last-fm/last-fm.txt',
		function(feed) {
			var feedHTML = '',
					tracks = feed['recenttracks']['track'],
					count = 1;
			for (var i = 0; i < limit; i++) {
				var artist = tracks[i].artist['#text'],
						// title = tracks[i].name,
						album = tracks[i].album['#text'];

				feedHTML += '';

				feedHTML += '<p class="item__title">'+artist+'</p>';
				feedHTML += '<p>'+album+'</p>';

			}
	target.html(feedHTML);
		});
})(jQuery);