(function(b) {
  b.fn.cmbConditionalShow = function(e, f) {
    b(this).on("change", e, function(c) {
      var a = b(this);
      c = a.val();
      for (var a = a.closest(".cmb-single"), g = a.siblings(".cmb__conditional"), d = 0, e = f.length;d < e;d++) {
        if (f[d].value === c) {
          c = a.siblings(f[d].c_target);
          g.removeClass("condition--visible").addClass("condition--hidden").val("");
          c.removeClass("condition--hidden").addClass("condition--visible");
          break;
        } else {
          g.removeClass("condition--visible").addClass("condition--hidden").val("");
        }
      }
    });
  };
})(jQuery);

jQuery('.cmb-repeat-group-wrap').cmbConditionalShow('.section__type select', [{ "value": "block--banner", "c_target": ".banner__extra" }, { "value": "block--definition", "c_target": ".definition__extra" }, { "value": "block--cta", "c_target": ".cta__extra"}]);

(function(c) {
  c.fn.extend( {
    limiter: function(limit) {
      var counter = "<p class='chars'></p>";
      c(this).after(counter);
      var closest = c(this).siblings('.chars');
      c(this).on("keyup focus", function() {
          setCount(this, closest);
      });
      function setCount(src) {
          var chars = src.value.length;
          if (chars > limit) {
              closest.css("color", "red");
          } else {
            closest.removeAttr("style");
          }
          closest.html( limit - chars );
      }
      setCount(c(this)[0], closest);
    }
  });
})(jQuery);

jQuery("#_rah_page_desc").limiter(100);
