// JQuery Twitter Feed. Coded by www.webdevdoor.com (2012) and modified from https://twitter.com/javascripts/blogger.js
(function ($) {

    var themename = "roadworkrah",
    twitterprofile = "roadworkrah",
    screenname = "RoadworkRah",
    displaylimit = 1,
    showdirecttweets = false,
    showretweets = false,
    showtweetlinks = false,
    showprofilepic = false,
    urlBuild = window.location.protocol + "//" + window.location.host + "/";

    jQuery.getJSON(urlBuild+'wp-content/themes/'+themename+'/library/'+twitterprofile+'-tweets.txt',
        function(feeds) {
            //alert(feeds);
            var feedHTML = '';
            var displayCounter = 1;
            for (var i=0; i<feeds.length; i++) {
                var tweetscreenname = feeds[i].user.name;
                var tweetusername = feeds[i].user.screen_name;
                var profileimage = feeds[i].user.profile_image_url_https;
                var status = feeds[i].text;
                var isaretweet = false;
                var isdirect = false;
                var tweetid = feeds[i].id_str;

                status = status.replace(/(http[s]\:\/\/[A-Za-z0-9\/\.\?\=\-]*)/g,'<a href="$1" target="_blank">$1</a>');
                status = status.replace(/@([A-Za-z0-9\/_]*)/g,'<a href="http://twitter.com/$1" target="_blank">@$1</a>');
                status = status.replace(/#([A-Za-z0-9\/\.]*)/g,'<a href="http://twitter.com/search?q=$1" target="_blank">#$1</a>');

                //If the tweet has been retweeted, get the profile pic of the tweeter
                if(typeof feeds[i].retweeted_status != 'undefined'){
                   profileimage = feeds[i].retweeted_status.user.profile_image_url_https;
                   tweetscreenname = feeds[i].retweeted_status.user.name;
                   tweetusername = feeds[i].retweeted_status.user.screen_name;
                   tweetid = feeds[i].retweeted_status.id_str
                   isaretweet = true;
                 };

                 //Check to see if the tweet is a direct message
                 if (feeds[i].text.substr(0,1) == "@") {
                     isdirect = true;
                 }

                //console.log(feeds[i]);

                 if (((showretweets == true) || ((isaretweet == false) && (showretweets == false))) && ((showdirecttweets == true) || ((showdirecttweets == false) && (isdirect == false)))) {
                    if ((feeds[i].text.length > 1) && (displayCounter <= displaylimit)) {
                        if (showtweetlinks == true) {
                            status = addlinks(status);
                        }

                        feedHTML += '<div class="feed__item">';
                        feedHTML += '<p class="feed__content">'+status+'</p>';
                        feedHTML += '<a href="https://twitter.com/'+tweetusername+'/status/'+tweetid+'" class="meta__time" target="_blank">'+relative_time(feeds[i].created_at)+'</a>';
                        feedHTML += '</div>';
                        displayCounter++;
                    }
                 }
            }

            jQuery('#js--twitter-feed').html(feedHTML);
    });

    function relative_time(tdate) {

          var system_date = new Date(Date.parse(tdate));
          var user_date = new Date();

          if (K.ie) {
              system_date = Date.parse(tdate.replace(/( \+)/, ' UTC$1'))
          }

          var diff = Math.floor((user_date - system_date) / 1000);
          if (diff <= 1) {return "just now";}
          if (diff < 20) {return diff + " seconds ago";}
          if (diff < 40) {return "half a minute ago";}
          if (diff < 60) {return "less than a minute ago";}
          if (diff <= 90) {return "one minute ago";}
          if (diff <= 3540) {return Math.round(diff / 60) + " minutes ago";}
          if (diff <= 5400) {return "1 hour ago";}
          if (diff <= 86400) {return Math.round(diff / 3600) + " hours ago";}
          if (diff <= 129600) {return "1 day ago";}
          if (diff < 604800) {return Math.round(diff / 86400) + " days ago";}
          if (diff <= 1123200) {return "1 week ago";}
                if (diff <= 1728000) {return "2 weeks ago";}
                if (diff <= 2332800) {return "3 weeks ago";}
                if (diff <= 5097600) {return "1 month ago";}
                if (diff <= 34214400) {return Math.round(diff / 3110400) + " months ago";}
          return "last year";

        }

        // from http://widgets.twimg.com/j/1/widget.js
        var K = function () {
          var a = navigator.userAgent;
          return {
            ie: a.match(/MSIE\s([^;]*)/)
            }
        }();

})(jQuery);