(function( $ ){

  $('body').on('click touchstart', function(e) {
    e.preventDefault();
    if ($('.js--modal').is(e.target)) {
      var d = window.location.protocol + "//" + window.location.host + "/wp-content/themes/nathanb/library/ajax/commits.php";
      var err_msg = "<p class=\"item-status--error\">This content couldn't be loaded. View the commit history here: <a href=\"https://github.com/NathanBurkett/nathanb-theme/\">https://github.com/NathanBurkett/nathanb-theme/<a></p>";
          $.ajax({
              type : "GET",
              // data : {numPosts: b, postType: a},
              dataType : "html",
              url : d,

              success : function(data){
                  $data = $(data);
                  var cont = "<div class='container--modal'></div>";
                  if($data.length){
                    $data.hide();
                    $('#container').addClass('modal--visible');
                    $(cont).appendTo("#container").hide().fadeIn(250, function() {
                      $data.appendTo(this).fadeIn(250);
                      $('.block--modal .social__item').each(function(i) {
                        $(this).css({'opacity': 0, 'margin-top' : '-0.75em'});
                        $(this).delay((i++) * 100).animate({'opacity': '1', 'margin-top' : 0}, 250, function() {
                          $(this).removeAttr("style");
                        });
                      });
                    });

                  } else {
                      t.append(err_msg);
                  }
              },
              error : function(jqXHR, textStatus, errorThrown){
                  t.append(err_msg);
                  console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
              }
          });
      }

      if (($('#container').hasClass('modal--visible')) && ((!$(".block--modal").is(e.target)) || ($(".modal__close").is(e.target)))) {
        $(".block--modal").fadeOut(250, function() {
          $(this).remove();
          $(".container--modal").fadeOut(250, function() {
            $(this).remove();
          });
        });
      }

  });

}( jQuery ));