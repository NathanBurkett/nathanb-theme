(function($) {

	var themename = "nathanb",
	// other vars
	target = $('#js--instagram'),
	limit = 18,
	dirBuild = window.location.protocol + "//" + window.location.host + "/";

	$.getJSON(dirBuild+'wp-content/themes/'+themename+'/library/api/instagram/instagram.txt',
		function(feed) {
			var data = feed['data'],
					total_num = data.length,
					feedHTML = '',
					count = 1;

					if (total_num < limit) {
						limit = total_num - (total_num % 6);
					}

			for (var i = 0; i < limit; i++) {
				var caption = '',
						img_url = data[i].images.standard_resolution.url,
						caption = data[i].caption,
						caption_text = caption != null ? caption.text : '';
						date = data[i].created_time,
						url = data[i].link,
						likes = data[i].likes.count;
				feedHTML += i % 6 === 0 ? '<div class="row--flex">' : '';

				feedHTML += '<li class="list__item gallery__item js--load__block social__item"><a class="item__link" href="'+url+'">';
				feedHTML += '<img class="gallery__img" src ="'+img_url+'" alt="'+caption+' - Follow Nathan Burkett on Instagram">';
				// feedHTML += '<p class="item__meta meta--likes">'+likes+'</p>';
				feedHTML += caption != null ? '<p class="item__description">'+caption_text+'</p>' : '';
				// feedHTML += '<p class="item__meta meta--time">'+date+'</p>';
				feedHTML += '</a></li>';

				feedHTML += i % 6 === 5 ? '</div>' : '';

			}

	target.html(feedHTML);

		});


})(jQuery);