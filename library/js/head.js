jQuery(document).ready(function($) {
	jQuery('.js--load__block').css({'opacity': 0});
	jQuery(window).load(function() {
		jQuery('.js--load__block').each(function(i) {
		  jQuery(this).css({'opacity': 0, 'margin-top' : '-0.75em'});
		  jQuery(this).delay((i++) * 200).animate({'opacity': '1', 'margin-top' : 0}, 350, function() {
		    jQuery(this).removeAttr("style");
		  });
		});
	});
});