<?php

$parse = parse_url(get_home_url());

$domain = $parse['host'];

class roadwork_Admin {

	private $key = 'roadwork_options';

	protected $option_metabox = array();



	protected $title = '';


	protected $options_page = '';

	public function __construct() {
		// Set our title
		$page__title_attrib = __( 'Options', 'roadwork' );
		$page__title = get_bloginfo('name');
		$page__title .= ' ' . $page__title_attrib;
		$this->title = $page__title;
	}

	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
	}

	public function init() {
		register_setting( $this->key, $this->key );
	}


	public function add_options_page() {
		$this->options_page = add_menu_page( $this->title, 'Site Options', 'manage_options', $this->key, array( $this, 'admin_page_display' ) );
	}

	public function admin_page_display() {
		?>
		<div class="wrap cmb_options_page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php cmb_metabox_form( self::option_fields(), $this->key ); ?>
		</div>
		<?php
	}

	public function option_fields() {

		// Only need to initiate the array once per page-load
		if ( ! empty( $this->option_metabox ) ) {
			return $this->option_metabox;
		}

		$prefix = '_rah_';

		$this->fields = array(
			array(
				'name' => __('Site Logo', 'roadwork'),
				'id' => $prefix . 'admin__logo',
				'type' => 'file',
				'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
			),
			array(
				'name' => __('Twitter', 'roadwork'),
				'desc' => __('These are your twitter custom inputs', 'roadwork'),
				'type' => 'title',
				'id' => $prefix . 'twitter_title'
			),
			// handle
			array(
				'name' => __('Handle', 'roadwork'),
				'desc' => __('Your Twitter username', 'roadwork'),
				'id' => $prefix . 'twitter_handle',
				'type' => 'text_medium',
				'attributes'  => array(
					'placeholder' => __('@user_name', 'roadwork'),
				),
			),
			// consumer key
			array(
				'name' => __('Consumer Key', 'roadwork'),
				'default' => '',
				'id' => $prefix . 'twitter_consumer_key',
				'type' => 'text'
			),
			// consumer secret
			array(
				'name' => __('Consumer Secret', 'roadwork'),
				'default' => '',
				'id' => $prefix . 'twitter_consumer_secret',
				'type' => 'text'
			),
			// access token
			array(
				'name' => __('Access Token', 'roadwork'),
				'default' => '',
				'id' => $prefix . 'twitter_access_token',
				'type' => 'text'
			),
			// access token secret
			array(
				'name' => __('Access Token Secret', 'roadwork'),
				'default' => '',
				'id' => $prefix . 'twitter_access_token_secret',
				'type' => 'text'
			),
			// # of tweets
			array(
				'name' => __('# of Tweets to Show', 'roadwork'),
				'desc' => __('10 max.', 'cmb'),
				'default' => '5',
				'id' => $prefix . 'twitter_feed_num',
				'type' => 'text_small'
			),
			array(
				'name' => __('Google Analytics', 'roadwork'),
				'type' => 'title',
				'id' => $prefix . 'ga_title'
			),
			array(
				'name' => __('Account', 'roadwork'),
				'id' => $prefix . 'ga_account',
				'type' => 'text_medium',
				'attributes'  => array(
					'placeholder' => __('UA-XXXX-Y', 'roadwork'),
				),
			),
			array(
				'id' => $prefix . 'social_group',
				'type' => 'group',
				'description' => __( 'Select social network profiles', 'cmb' ),
				'options' => array(
					'group_title' => __( 'Social Network {#}', 'cmb' ), // since version 1.1.4, {#} gets replaced by row number
					'add_button' => __( 'Add Another Social Network', 'cmb' ),
					'remove_button' => __( 'Remove Social Network', 'cmb' ),
					'sortable' => true, // beta
				),
				// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
					'fields' => array(
						array(
						    'name' => __('Social Network', 'cmb'),
						    'id' => $prefix . 'social_select',
						    'type' => 'select',
						    'options' => array(
						    		'select' => __('Select a Social Network', 'cmb'),
						        'facebook' => __( 'Facebook', 'cmb' ),
						        'linkedin' => __( 'LinkedIn', 'cmb' ),
						        'pinterest' => __( 'Pinterest', 'cmb' ),
						    ),
						    'default' => 'select',
						),
						array(
						    'name' => __( 'Social Network Profile URL', 'cmb' ),
						    'id' => $prefix . 'social_url',
						    'type' => 'text_url',
						    // 'protocols' => array( 'http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet' ), // Array of allowed protocols
						),
					),
				),
		);

		$this->option_metabox = array(
			'id'         => 'option_metabox',
			'show_on'    => array( 'key' => 'options-page', 'value' => array( $this->key, ), ),
			'show_names' => true,
			'fields'     => $this->fields,
		);

		return $this->option_metabox;
	}

	public function __get( $field ) {

		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'fields', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}
		if ( 'option_metabox' === $field ) {
			return $this->option_fields();
		}

		throw new Exception( 'Invalid property: ' . $field );
	}

}

// Get it started
$roadwork_Admin = new roadwork_Admin();
$roadwork_Admin->hooks();

function get_admin_option( $key = '' ) {
	global $roadwork_Admin;
	return cmb_get_option( $roadwork_Admin->key, '_rah_' . $key );
}