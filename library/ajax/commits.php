<?php

$commits_json = file_get_contents('../api/github/github.txt');

if ($commits_json) :

	date_default_timezone_set('America/Denver');

	echo '<div class="block--modal is--visible social--github">';

	echo '<header class="section__header">';

		echo '<h3 class="section__title">NathanB Theme</h3>';

		echo '<a href="https://github.com/NathanBurkett/nathanb-theme/" class="section__content">https://github.com/NathanBurkett/nathanb-theme/</a>';

		echo '<svg class="icon icon--github"><use xlink:href="http://dev.nathanb.me/wp-content/themes/nathanb/library/images/icons.svg#icon_github"></use></svg>';

	echo '</header>';

	$commits = json_decode($commits_json, true);

	foreach ($commits as $commit) :

		$date = date('D, M jS \a\t g:ia', strtotime($commit['date']));

		echo '<div class="social__item">';

			echo '<p class="item__title">',$commit['desc'],'</p>';
			echo '<p class="item__meta meta--time">',$date,'</p><a class="item__link" href="',$commit['url'],'">',substr($commit['commit_id'], 0, 10),'</a>';

		echo '</div>';

	endforeach;

	echo '</div>';

endif;

?>