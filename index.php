<?php	get_template_part( 'scaffold/standard', 'start' ); ?>

	<section class="row page__content content--main">

		<div class="content--column js--load__block">

			<section class="content__block">

				<header class="section__header"><h2 class="section__title">Technical Skills</h2></header>

				<ul class="list--unordered" itemtype="http://schema.org/ItemList">

					<li class="list__item" itemprop="itemListElement">Computer-literate professional with a proficiency in programming languages HTML5, CSS3, PHP, and jQuery</li>
					<li class="list__item" itemprop="itemListElement">Highly experienced human interaction designer with an understanding of industry standard tools like Adobe Illustrator and Photoshop</li>
					<li class="list__item" itemprop="itemListElement">Seasoned with on-the-cusp technology such as responsive design, task running, and preprocessors</li>
					<li class="list__item" itemprop="itemListElement">Capable of building and framing concepts to create, match, or continue a brand</li>
					<li class="list__item" itemprop="itemListElement">Current with schema data and rich snippet standards as it relates to search engine optimization</li>

				</ul>

			</section>

			<section class="content__block">

				<header class="section__header"><h2 class="section__title">Personal Skills</h2></header>

				<ul class="list--unordered" itemtype="http://schema.org/ItemList">

					<li class="list__item" itemprop="itemListElement">Highly analytical thinker with demonstrated talent for identifying, scrutinizing, improving, and streamlining complex work processes</li>
					<li class="list__item" itemprop="itemListElement">Exceptional listener and communicator with the ability to effectively convey information</li>
					<li class="list__item" itemprop="itemListElement">Results-driven achiever with exemplary planning and organizational skills, along with a high degree of detail orientation</li>
					<li class="list__item" itemprop="itemListElement">Productive worker with solid work ethic who thrives in environments requiring an ability to effectively prioritize and juggle multiple concurrent projects</li>

				</ul>

			</section>

		</div>

		<div class="content--column js--load__block">

			<section class="content__block">

				<header class="section__header"><h2 class="section__title">Experience</h2></header>

				<section class="content--description" itemscope itemtype="http://schema.org/Organization">

					<header class="content__header">

						<h3 class="content__title" itemprop="name">Roadwork Rah</h3>

						<p class="content__meta">Feb. 2012 to Present</p>

						<p class="content__subtitle" itemprop="jobTitle">Founder / Lead Designer / Lead Developr</p>

					</header>

					<p class="desctiption__content" itemprop="description">Nathan's duties at Roadwork Rah included client relations and retention, brand direction, creation, and implementation, design and development of websites, maintaining code bases, print and marketing material creation, cost allocation and time tracking, Wordpress custom theme development, day-to-day operations, and long-term strategy.</p>

					<!-- <ul class="list--single" itemtype="http://schema.org/ItemList"><li class="list__item" itemprop="itemListElement"></li></ul> -->

				</section>

				<section class="content--description" itemscope itemtype="http://schema.org/Organization">

					<header class="content__header">

						<h3 class="content__title" itemprop="name">Epik Eyewear</h3>

						<p class="content__meta">Sept. 2010 to Feb. 2012</p>

						<p class="content__subtitle" itemprop="jobTitle">Operations Director / Creative Lead</p>

					</header>

					<p class="desctiption__content" itemprop="description">Nathan's duties at Epik Eyewear included supply chain management, distributor and manufacturer relations, website build and maintenance, brand direction and implementation, prototype oversight, and long-term strategy.</p>

					<!-- <ul class="list--single" itemtype="http://schema.org/ItemList"><li class="list__item" itemprop="itemListElement"></li></ul> -->

				</section>

			</section>

			<section class="content__block block--education">

				<header class="section__header"><h2 class="section__title">Education</h2></header>

				<section class="content--description" itemscope itemtype="http://schema.org/EducationalOrganization">

					<h3 class="content__title title--alt" itemprop="name">University of Colorado</h3>

					<address itemprop="address">Boulder, CO</address>

					<a href="http://www.colorado.edu" itemprop="url">colorado.edu</a>

					<p class="content--focus"><strong>Economics</strong></p>

					<p class="content__meta">2005-2009</p>

				</section>

			</section>

		</div>

	</section>

	<section class="row--full-width page__content content--social">

		<div class="row">

		<section class="block--social js--load__block social--github">

			<header class="section__header"><h2 class="section__title"><span>github.com/</span>NathanBurkett</h2></header>

			<ul id="js--github" class="container--loading">

				<span class="media--loader"></span>

			</ul>

		</section>

		<section class="block--social js--load__block social--single">

			<header class="section__header"><h2 class="section__title">@nathan_burkett</h2></header>

			<?php get_template_part('library/api/twitter/twitter.output'); ?>

		</section>

		<section class="block--social js--load__block social--lastfm">

			<img width="110" class="social__img" src="<?php echo get_template_directory_uri().'/library/images/audiotune.gif'; ?>" alt="Nathan Burkett's Groovin' Img">

			<div class="social__content">

				<p>Currently listening to</p>

				<section id="js--last-fm" class="item__content"></section>

			</div>

		</section>

		</div>

		<div class="row--full-width block--social social--gallery">

			<ul id="js--instagram" class="list--gallery container--loading">

				<span class="media--loader"></span>

			</ul>

		</div>

	</section>

<?php get_template_part( 'scaffold/standard', 'end'); ?>