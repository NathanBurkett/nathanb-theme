module.exports = function(grunt) {

    // 1. All configuration goes here
    grunt.initConfig({
      jcd: 'library/js/',
      scd: 'library/scss/',
      ccd: 'library/css/',
      watch: {
        options: {
          spawn: false,
        },
          css: {
            files: ['<%= scd %>01-generic/**/*.scss', '<%= scd %>02-base/**/*.scss', '<%= scd %>03-objects/**/*.scss'],
            tasks: ['sass:dev']
          },
          js: {
            files: '<%= jcd %>**/*.js',
            tasks: ['concat', 'jshint'],
          }
      },
      concat: {
        head: {
          src: ['<%= jcd %>functions/loadAnimation.js'],
          dest: '<%= jcd %>head.js',
        },
        foot: {
          src: ['<%= jcd %>functions/converEmails.js','<%= jcd %>functions/relDate.js', '<%= jcd %>libs/ajax.gitHubCommits.js', '<%= jcd %>libs/getGithubApi.js', '<%= jcd %>libs/getTwitterApi.js', '<%= jcd %>libs/getInstagramApi.js', '<%= jcd %>libs/getLastFMApi.js'],
          dest: '<%= jcd %>foot.js',
        },
      },
      uglify: {
        build: {
          files: {
            '<%= jcd %>head.min.js': '<%= jcd %>head.js',
            '<%= jcd %>foot.min.js' : '<%= jcd %>foot.js'
          }
        }
      },
      sass: {
        dev: {
          options: {
              style: 'expanded',
              sourcemap: true,
          },
          expand: true,
          flatten: true,
          files: {
            '<%= ccd %>style.css': '<%= scd %>style.scss'
            // '<%= ccd %>style-guide.css': '<%= scd %>style-guide.scss'
          }
        },
        prod: {
          options: {
              style: 'compressed',
          },
          files: {
            '<%= ccd %>style.css': '<%= scd %>style.scss',
            '<%= ccd %>style-guide.css': '<%= scd %>style-guide.scss'
          }
        },
      },
      autoprefixer: {
        default: {
          options: {
              map: true
          },
          // expand: true,
          // flatten: true,
          src: '<%= ccd %>style.css' // globbing is also possible here
        },
        styleguide: {
          src: '<%= ccd %>style-guide.css' // globbing is also possible here
        },
      },
      cmq: {
        options: {
          log: false
        },
        your_target: {
            files: {
              '<%= ccd %>style.css': ['<%= ccd %>style.css']
            },
        },
      },
      cssmin: {
        min_css: {
          files: {
            '<%= ccd %>style.min.css': ['<%= ccd %>style.css']
          }
        }
      },
      jshint: {
        all: '<%= jcd %>/**/*.js',
      },
      svgstore: {
        options: {
          prefix : '', // This will prefix each ID
        },
        default : {
          files: {
            'library/images/icons.svg': 'library/images/svg/*.svg',
          },
        },
      },
    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-combine-media-queries');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-svgstore');
    // grunt.loadNpmTasks('grunt-autoshot');
    // grunt.loadNpmTasks('grunt-imageoptim');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.

    grunt.registerTask('sassDev', ['sass:dev', 'watch:css']);
    grunt.registerTask('jsDev', ['concat', 'jshint', 'watch:js']);
    grunt.registerTask('prodEnv', ['sass:prod', 'autoprefixer:default', 'cmq', 'cssmin', 'concat', 'jshint', 'uglify', 'imageoptim']);
    // grunt.registerTask('autoShot', ['autoshot']);
    // grunt.registerTask('imageOptim', ['imageoptim']);
    // grunt.registerTask('favicon', ['shell']);
    grunt.registerTask('svg', ['svgstore']);

};